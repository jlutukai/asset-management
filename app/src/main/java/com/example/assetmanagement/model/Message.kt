package com.example.assetmanagement.model

data class Message(
    val error : String,
    val message : String
)