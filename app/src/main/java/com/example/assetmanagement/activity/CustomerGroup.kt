package com.example.assetmanagement.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.example.assetmanagement.adapter.ContactGroupsAdapter
import com.example.assetmanagement.model.ContactGroup
import com.example.assetmanagement.room.viewModel.GeneralViewModel
import com.example.assetmanagement.utils.isNetworkConnected
import com.example.assetmanagement.utils.showToast
import kotlinx.android.synthetic.main.activity_customer_group.*

class CustomerGroup : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var contactList : MutableList<ContactGroup>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_group)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        rv_contact_group.layoutManager = LinearLayoutManager(this)
        rv_contact_group.isNestedScrollingEnabled = false

        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getContactGroups()
    }

    private fun getContactGroups() {
        contactList = arrayListOf()
        if (isNetworkConnected(this)){
            generalViewModel.getUpdatedContactGroups()
        }else{
            showToast("from Room db")
        }
        generalViewModel.getLocalContactGroups().observe(this@CustomerGroup, Observer {contactGroups ->
            if (contactGroups.isNotEmpty()){
                contactList.clear()
                progress.visibility = View.GONE
                for (n in contactGroups){
                    contactList.add(n)
                }
                val i = contactList.lastIndexOf(ContactGroup())
                contactList.add(i+1, ContactGroup(900,"Customers","00:00"))
                val contactGroupsAdapter = ContactGroupsAdapter(this, contactList)
                rv_contact_group.adapter = contactGroupsAdapter
            }else{
                showToast("No Contact Groups Available")
            }

        })
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
