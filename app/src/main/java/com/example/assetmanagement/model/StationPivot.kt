package com.example.assetmanagement.model

data class StationPivot(
    val user_id : Int?,
    val business_location_id : Int?
)