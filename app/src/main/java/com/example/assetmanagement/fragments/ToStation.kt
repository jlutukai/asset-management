package com.example.assetmanagement.fragments


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.assetmanagement.R
import com.google.gson.Gson
import com.example.assetmanagement.activity.CaptureBeacons
import com.example.assetmanagement.activity.CaptureNfc
import com.example.assetmanagement.ble.Utils
import com.example.assetmanagement.model.AllStations
import com.example.assetmanagement.model.Message
import com.example.assetmanagement.model.TransferReasons
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.isNetworkConnected
import com.example.assetmanagement.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_to_station.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class ToStation : Fragment() {
    private lateinit var stations: List<AllStations>
    private lateinit var allStations: List<AllStations>
    private lateinit var reasons : List<TransferReasons>
    private lateinit var uid : String
    private lateinit var loading: Dialog
    private var stationNames: MutableList<String> = arrayListOf()
    private var allStationNames: MutableList<String> = arrayListOf()
    private var reasonNames : MutableList<String> = arrayListOf()
    private var lat : String =""
    private var long : String = ""
    private val permissionRequestAccessFineLocation = 100

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_to_station, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loading = Dialog(context!!)
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loading.setCancelable(false)
        loading.setContentView(R.layout.loading_dialog)
        loading.show()
        getLocationDetails()
        getList()
        PushDownAnim.setPushDownAnimTo(add_assets_transfer).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            loading.show()
            getSelectedData()

        }
    }
    private fun getLocationDetails() {
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                this as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),permissionRequestAccessFineLocation)
            return
        }

        val locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val locationListener =  object  : LocationListener {

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {

            }

            override fun onLocationChanged(p0: Location?) {
                val latitude = p0!!.latitude
                lat = latitude.toString()
                val longitude = p0.longitude
                long = longitude.toString()
            }
        }


        locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestAccessFineLocation) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocationDetails()
                PackageManager.PERMISSION_DENIED -> context!!.showToast("enable location")
            }
        }
    }

    private fun getSelectedData() {
        val reason : String = reason_t.selectedItemPosition.toString()
        val stationName: String = station_atf.selectedItem.toString()
        var stid : String = ""
        var stid2 : String = " "
        for (i in stations){
            if (i.name + " ( " + i.landmark + " )" == stationName){
                stid = i.id.toString()
            }
        }
        val stationN : String = station_fsts.selectedItem.toString()
        for (i in allStations){
            if (i.name + " ( " + i.landmark + " )" == stationN){
                stid2 = i.id.toString()
            }
        }
        val refNo : String = ref_nots.text.toString()
        if(refNo.isEmpty()){
            loading.dismiss()
            ref_nots.error = "Required"
        }else{
            GlobalScope.launch(Dispatchers.Main) { addMovement(reason,stid, stid2,refNo) }
        }

    }

    private suspend fun addMovement(
        reason: String,
        stid: String,
        stid2: String,
        refNo: String
    ) {
        try {
            val response = ApiClient.webService.addMovement(context!!.getToken(),stid,stid2,uid,null,null,reason,refNo, long, lat)
            if (response.isSuccessful){
                loading.dismiss()
                val id : String = response.body()!!.movement.id.toString()
                showOptionsDialog(stid,id)

            }else{
                loading.dismiss()
                val gson = Gson()
                val errorResponse = gson.fromJson<Message>(response.errorBody()!!.charStream(),
                    Message::class.java)
                val msg = errorResponse.message
                context!!.showToast(msg)
            }
        }catch (e:IOException){
            loading.dismiss()
            e.message?.let { context!!.showToast(it) }
        }

    }

    private fun showOptionsDialog(stid: String, id: String) {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_options_tech)
        val close = dialog.findViewById<ImageView>(R.id.close_dialog_tech)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val nfc = dialog.findViewById<TextView>(R.id.nfc)
        PushDownAnim.setPushDownAnimTo(nfc).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(context!!, CaptureNfc::class.java)
                .putExtra("type", "addToMovement").
                    putExtra("stid", stid)
                .putExtra("id", id))
            dialog.dismiss()
        }
        val ble = dialog.findViewById<TextView>(R.id.beacons)
        PushDownAnim.setPushDownAnimTo(ble).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(context!!, CaptureBeacons::class.java)
                .putExtra("type", "addToMovement").
                    putExtra("stid", stid)
                .putExtra("id", id))
            dialog.dismiss()
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private fun getList() {
        if (isNetworkConnected(context!!)) {
            GlobalScope.launch(Dispatchers.Main) { getAllData() }
        } else {
            Utils.toast(activity, "Check Internet Connection")

        }
    }

    private suspend fun getAllData() {
        try {
            val response =  ApiClient.webService.getUserData(context!!.getToken())
            if (response.isSuccessful){
                uid = response.body()!!.user.id.toString()
            }
        }catch (e:IOException){
            e.message?.let { context!!.showToast(it) }
        }
        try{
            val response = ApiClient.webService.fetchReasons(context!!.getToken())
            if (response.isSuccessful){
                reasons = response.body()!!.transfer_reasons
                for (i in reasons){
                    reasonNames.add(i.reason)
                }
                val ab = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, reasonNames)
                ab.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                with(reason_t) {
                    adapter = ab
                    setSelection(0, false)
                    prompt = "Select Station"
                    gravity = Gravity.CENTER
                }
            }
        }catch (e:IOException){
            e.message?.let { context!!.showToast(it) }
        }
        try {
            val response = ApiClient.webService.fetchAssets(context!!.getToken())
            if (response.isSuccessful){
                allStations = response.body()!!.stations
                for (i in allStations){
                    allStationNames.add(i.name + " ( " + i.landmark + " )")
                }
                val aa = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, allStationNames)
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                with(station_fsts) {
                    adapter = aa
                    setSelection(0, false)
                    prompt = "Select Station"
                    gravity = Gravity.CENTER
                }
            }
            context!!.showToast(allStationNames.size.toString())
        }catch (e:IOException){
            e.message?.let { context!!.showToast(it) }
        }
        try {
            val response = ApiClient.webService.getUserData(context!!.getToken())
            if (response.isSuccessful) {
                if (response.body()!!.user.is_customer == 0) {
                    stations = response.body()!!.user.stations
                    for (i in stations) {
                        stationNames.add(i.name + " ( " + i.landmark + " )")
                    }
                    val ac = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, stationNames)
                    ac.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    with(station_atf) {
                        adapter = ac
                        setSelection(0, false)
                        prompt = "Select Station"
                        gravity = Gravity.CENTER
                    }

                }
            }
        } catch (e: IOException) {
            e.message?.let { context!!.showToast(it) }
        }
        loading.dismiss()
    }

}
