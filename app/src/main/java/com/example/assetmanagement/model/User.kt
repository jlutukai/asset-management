package com.example.assetmanagement.model



data class User(
    val id : Int,
    val surname : String,
    val first_name : String,
    val last_name : String,
    val username : String,
    val is_customer : Int,
    val business_id : Int,
    val email : String?,
    val stations: List<AllStations>
)
