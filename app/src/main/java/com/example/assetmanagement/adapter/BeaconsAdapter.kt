package com.example.assetmanagement.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.example.assetmanagement.ble.BTLEDevice
import kotlinx.android.synthetic.main.item_ble_device.view.*

class BeaconsAdapter (val context: Context, private val beacons: MutableList<BTLEDevice>) :
    RecyclerView.Adapter<BeaconsAdapter.BeaconsViewModel>(){
    var onItemClick: ((BTLEDevice) -> Unit)? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BeaconsViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_ble_device, parent, false)
        return BeaconsViewModel(v)
    }

    override fun getItemCount(): Int {
        return beacons.size
    }

    override fun onBindViewHolder(holder: BeaconsViewModel, position: Int) {
        val beacon = beacons[position]
        holder.setData(beacon, position)



    }

    inner class BeaconsViewModel(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var current: BTLEDevice? = null
        private var currentPos: Int = 0

        init {
            itemView.item.setOnClickListener {
                current?.let {
                    onItemClick!!.invoke(it)
                }
//                val pos = currentPos
//                beacons[pos].checked = true
//                notifyDataSetChanged()
            }


        }


        fun setData(beacon: BTLEDevice, position: Int) {
            beacon.let {
                val distance = beacon.dist.toString()
                val d :String
                d = if (distance.length > 4){
                    distance.substring(0,4)
                }else{
                    distance
                }
                itemView.dist.text = "$d m"
//                itemView.major.text = "Major : "+beacon.major
                itemView.minor.text = beacon.minor
                itemView.tv_macaddr.text = "Address : "+beacon.address
                if (beacon.checked){
                    itemView.item.setBackgroundColor(ContextCompat.getColor(context,R.color.green_50))
                }else if (!beacon.checked){
                    itemView.item.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
                }
            }

            current = beacon
            currentPos = position
        }

    }

}