package com.example.assetmanagement.room.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.assetmanagement.model.*
import com.example.assetmanagement.room.repository.GeneralRepository

class GeneralViewModel(application: Application) : AndroidViewModel(application) {
    private val generalRepo = GeneralRepository()



    //change status
//    fun changeStatus(){
//        generalRepo.change(application = getApplication())
//    }

    //contact
    fun getLocalContacts():LiveData<List<Contact>>{
        return generalRepo.localContacts()
    }
    fun getUpdatedContacts(){
        generalRepo.apiContacts(application = getApplication())
    }

    //contact groups
    fun getLocalContactGroups() : LiveData<List<ContactGroup>>{
        return generalRepo.localContactGroups()
    }
    fun getUpdatedContactGroups(){
        generalRepo.apiContactGroups(application = getApplication())
    }

    //categories
    fun getLocalCategories(): LiveData<List<Category>> {
        return generalRepo.localCategories()
    }
    fun getUpdatedCategories() {
        generalRepo.apiCategories(application = getApplication())
    }

    //products
    fun getLocalProducts(): LiveData<List<Product>> {
        return generalRepo.localProducts()
    }
    fun getUpdatedProducts() {
        generalRepo.apiProducts(application = getApplication())
    }

    //SALES
    fun roomItems(salesLocal: SalesLocal){
        generalRepo.roomDbItems(salesLocal)
    }

    //property types

}