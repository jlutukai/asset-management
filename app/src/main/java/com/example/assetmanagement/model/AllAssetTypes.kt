package com.example.assetmanagement.model

import androidx.room.Entity

@Entity(tableName = "assettypes")
data class AllAssetTypes(
    val id : Int,
    val name : String,
    val description : String,
    val created_at : String,
    val updated_at : String
)