package com.example.assetmanagement.ble;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.assetmanagement.activity.CaptureBeacons;

import java.text.DecimalFormat;


public class Utils {
    private static CaptureBeacons captureBeacons = new CaptureBeacons();
    public static boolean checkBluetooth(BluetoothAdapter bluetoothAdapter){
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }
    public static void requestUserBluetooth(Activity activity){
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        activity.startActivityForResult(enableBtIntent, captureBeacons.REQUEST_ENABLE_BT);
    }
    public static void toast  (Context context, String string){
        Toast toast = Toast.makeText(context, string, Toast.LENGTH_SHORT);
        toast.show();
    }
    public static double calculateAccuracy(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double dis = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            DecimalFormat df = new DecimalFormat("#.##");
            return Double.parseDouble(df.format(dis));
        }
    }
}
