package com.example.assetmanagement.model

data class TransferReasons(
    val id : Int,
    val reason : String
)
