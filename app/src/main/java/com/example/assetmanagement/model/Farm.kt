package com.example.assetmanagement.model

data class Farm(
    val id: Int,
    val business_id: Int,
    val contact_id: Int,
    val total_area: Double,
    val area_cultivated: Double,
    val created_at: String,
    val updated_at: String,
    val location_id: Int,
    val address: String,
    val lr_number: String,
    val zone_id: Int,
    val contact: Contact?,
    val location: Location,
    val zone: Zone
)