package com.example.assetmanagement.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.assetmanagement.R
import com.example.assetmanagement.model.AllStations
import com.example.assetmanagement.model.Contact
import com.example.assetmanagement.model.Units
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.isNetworkConnected
import com.example.assetmanagement.utils.showToast
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_add_batch.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.IOException

class AddBatch : AppCompatActivity() {
    private val permissionRequestAccessFineLocation = 100
    private var supplier: MutableList<String> = arrayListOf()
    private var units: MutableList<String> = arrayListOf()
    private var station: MutableList<String> = arrayListOf()
    private lateinit var allsuppliers: List<Contact>
    private lateinit var allunits: List<Units>
    private lateinit var allstations: List<AllStations>
    private lateinit var intervals: List<String>
    private lateinit var contactid: String
    private lateinit var unitid: String
    private lateinit var stationid: String
    private lateinit var interval: String
    private var lat : String =""
    private var long : String = ""
    private lateinit var imageuri : Uri
    var imageresult : ArrayList<String> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_batch)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        getLists()
        warranty.setOnClickListener {
        if (warranty.isChecked) {
            warranty_details.visibility = View.VISIBLE
        }else{
            warranty_details.visibility = View.GONE
        }
        }
        getLocationDetails()
        PushDownAnim.setPushDownAnimTo(add_new_batch).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                val batchno: String = b_no.text.toString()
                val dur: String = duration.text.toString()
                if (warranty.isChecked && dur.isEmpty()) {
                    duration.error = "Required"
                } else {
                    getDetails()
                }
            }

        image.setOnClickListener {
            Pix.start(this, Options.init().setRequestCode(100))
        }
    }

    private fun getLocationDetails() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                this as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),permissionRequestAccessFineLocation)
            return
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val locationListener =  object  : LocationListener {

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {

            }

            override fun onLocationChanged(p0: Location?) {
                val latitude = p0!!.latitude
                lat = latitude.toString()
                val longitude = p0.longitude
                long = longitude.toString()
            }
        }


        locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestAccessFineLocation) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocationDetails()
                PackageManager.PERMISSION_DENIED -> showToast("enable location")
            }
        }
    }

    private fun getDetails() {
        val supplierName: String = supplier_name.selectedItem.toString()
        for (i in allsuppliers) {
            if (i.name+" of "+ i.supplier_business_name == supplierName || i.name == supplierName) {
                contactid = i.id.toString()
            }
        }
        val unit: String = unit.selectedItem.toString()
        for (i in allunits) {
            if (i.actual_name == unit) {
                unitid = i.id.toString()
            }
        }
        val station: String = station_t.selectedItem.toString()
        for (i in allstations) {
            if (station == i.name + " (" + i.landmark + ")") {
                stationid = i.id.toString()
            }
        }

        interval = interval_t.selectedItemPosition.toString()

        if (isNetworkConnected(this)) {
//            generalViewModel.getUpdatedAssetTypes()
            GlobalScope.launch(Dispatchers.Main) { newBatch() }
        } else {
            progress.visibility = View.GONE
            showToast("Check Internet Connection")
        }
    }

    private suspend fun newBatch() {
        val id: String? = intent.getStringExtra("id")
        val batchno: String = b_no.text.toString()
        val desc : String = description.text.toString()
//        val file  = File(imageuri.toString())
        val dur: String = if (warranty.isChecked) {
            duration.text.toString()
        } else {
            ""
        }
        val inter: String = if (warranty.isChecked) {
            interval
        } else {
            ""
        }

        val warr: Int = if (warranty.isChecked) {
            1
        } else {
            0
        }

        try {
            val response = ApiClient.webService.addBatch(
                getToken(),desc, batchno, id!!, unitid,
                contactid, warr, dur, inter, stationid
            ,long,lat
            )
            if (response.isSuccessful) {
                showToast("$batchno has been added successfully")
                startActivity(Intent(this, Batches::class.java).putExtra("id", id))
                finish()
            }else{
                showToast("Failed : ${response.code()}")
            }
        } catch (e: IOException) {
            //error
            showToast("Failed : ${e.message}")
        }

    }

    private fun getLists() {

        if (isNetworkConnected(this)) {
//            generalViewModel.getUpdatedAssetTypes()
            GlobalScope.launch(Dispatchers.Main) { lists() }
        } else {
            progress.visibility = View.GONE
            showToast("Check Internet Connection")
        }
    }

    private suspend fun lists() {

        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful) {
                allsuppliers = response.body()!!.suppliers
                for (i in allsuppliers) {
                    if (i.supplier_business_name.isNullOrEmpty()) {
                        supplier.add(i.name)
                    } else {
                        supplier.add(i.name+" of "+ i.supplier_business_name)
                    }
                }
                val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, supplier)
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                with(supplier_name) {
                    adapter = aa
                    setSelection(0, false)
                    prompt = "Select Supplier"
                    gravity = Gravity.CENTER
                }

                allunits = response.body()!!.units
                for (i in allunits) {
                    units.add(i.actual_name)
                }
                val ab = ArrayAdapter(this, android.R.layout.simple_spinner_item, units)
                ab.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                with(unit) {
                    adapter = ab
                    setSelection(0, false)
                    prompt = "Select unit"
                    gravity = Gravity.CENTER
                }
//
                allstations = response.body()!!.stations
                for (i in allstations) {
                    station.add(i.name + " (" + i.landmark + ")")
                }
                val ac = ArrayAdapter(this, android.R.layout.simple_spinner_item, station)
                ac.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                with(station_t) {
                    adapter = ac
                    setSelection(0, false)
                    prompt = "Select Station"
                    gravity = Gravity.CENTER
                }
//
                intervals = response.body()!!.intervals
                val ad = ArrayAdapter(this, android.R.layout.simple_spinner_item, intervals)
                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                with(interval_t) {
                    adapter = ad
                    setSelection(0, false)
                    prompt = "Select Interval"
                    gravity = Gravity.CENTER
                }
                progress.visibility = View.GONE
            }
        } catch (e: IOException) {

        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 100){
            imageresult = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!

            imageuri = Uri.fromFile(File(imageresult[0]))
            image.setImageURI(imageuri)

        }
    }


}
