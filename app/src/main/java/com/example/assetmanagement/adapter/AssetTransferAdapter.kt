package com.example.assetmanagement.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.example.assetmanagement.activity.ViewAssets
import com.example.assetmanagement.model.*
import kotlinx.android.synthetic.main.item_transfer.view.*

class AssetTransferAdapter(val context: Context,
                           private val transfers: List<Movements>,
                           private val stations : List<AllStations>,
                           private val contacts : List<Contact>,
                           private val status : List<String>,
                           private val id : Int?
                           ) :
    RecyclerView.Adapter<AssetTransferAdapter.AssetTransferViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetTransferViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_transfer, parent, false)
        return AssetTransferViewModel(v)
    }

    override fun getItemCount(): Int {
        return transfers.size
    }

    override fun onBindViewHolder(holder: AssetTransferViewModel, position: Int) {
        val transfer = transfers[position]
        holder.setData(transfer, position)
    }

    inner class AssetTransferViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Movements? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper_transfer.setOnClickListener {
                current.let {
                    val intent = Intent(context, ViewAssets::class.java)
                    intent.putExtra("id", current!!.id.toString() )
                    context.startActivity(intent)
                }
            }
        }

        fun setData(transfer: Movements, position: Int) {
            var assets : List<Assetss> = listOf()
            var count : String =""
            transfer.let {
                when (transfer.status) {
                    0 -> itemView.statecolor.setBackgroundColor(ContextCompat.getColor(context,R.color.purple_400))
                    1 -> itemView.statecolor.setBackgroundColor(ContextCompat.getColor(context,R.color.orange_400))
                    2 -> itemView.statecolor.setBackgroundColor(ContextCompat.getColor(context,R.color.green_400))
                    3 -> itemView.statecolor.setBackgroundColor(ContextCompat.getColor(context,R.color.red_400))
                    4 -> itemView.statecolor.setBackgroundColor(ContextCompat.getColor(context,R.color.yellow_400))
                }
                itemView.tracking.text = transfer.tracking_number
                assets = transfer.assets
                itemView.asset_count.text = assets.size.toString()
                itemView.ref.text = transfer.reference
                if (transfer.from_station_id != null){
                    for (i in stations){
                        if (i.id == transfer.from_station_id){
                            itemView.from.text = i.name
                        }
                    }
                }
                if (transfer.from_contact_id != null){
                    for (i in contacts){
                        if (i.id == transfer.from_contact_id){
                            itemView.from.text = i.name
                        }
                    }
                }
                if (transfer.to_station_id != null){
                    for (i in stations){
                        if (i.id == transfer.to_station_id){
                            itemView.to.text = i.name
                        }
                    }
                }
                if (transfer.to_contact_id != null){
                    for (i in contacts){
                        if (i.id == transfer.to_contact_id){
                            itemView.to.text = i.name
                        }
                    }
                }
                itemView.status.text = status[transfer.status]
                itemView.date.text = transfer.movement_date
            }
            current = transfer
            currentPos = position
        }
    }
}