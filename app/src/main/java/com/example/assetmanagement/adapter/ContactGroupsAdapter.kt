package com.example.assetmanagement.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.example.assetmanagement.activity.Contacts
import com.example.assetmanagement.model.ContactGroup
import kotlinx.android.synthetic.main.item_contact_group.view.*

class ContactGroupsAdapter(val context: Context, private val groups: List<ContactGroup>) :
    RecyclerView.Adapter<ContactGroupsAdapter.ContactGroupsViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactGroupsViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_contact_group, parent, false)
        return ContactGroupsViewModel(v)
    }

    override fun getItemCount(): Int {
        return groups.size
    }

    override fun onBindViewHolder(holder: ContactGroupsViewModel, position: Int) {
        val group = groups[position]
        holder.setData(group, position)
    }

    inner class ContactGroupsViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: ContactGroup? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper.setOnClickListener {
                current?.let {
                    val intent = Intent(context, Contacts::class.java)
                    intent.putExtra("id", current!!.id.toString())
                    context.startActivity(intent)
                }
            }
        }

        fun setData(group: ContactGroup, position: Int) {
            group.let {
                itemView.cust_group.text = group.name
            }
            current = group
            currentPos = position
        }
    }
}