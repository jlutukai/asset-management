package com.example.assetmanagement.model

data class Type(
    val id : Int,
    val business_id : Int,
    val customer_group_id : Int,
    val title : String,
    val description : String
)