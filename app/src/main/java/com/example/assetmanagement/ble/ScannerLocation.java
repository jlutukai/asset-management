package com.example.assetmanagement.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;

import com.example.assetmanagement.activity.BeaconLocation;

public class ScannerLocation {

    private BeaconLocation beaconLocation ;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;

    private long scanPeriod;
    private int signalStrength;

    public ScannerLocation (BeaconLocation mainActivity, long scanPeriod, int signalStrength){
        beaconLocation = mainActivity;
        mHandler = new Handler();
        this.scanPeriod = scanPeriod;
        this.signalStrength = signalStrength;
        final BluetoothManager bluetoothManager =
                (BluetoothManager) beaconLocation.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }

    public boolean isScanning() {
        return mScanning;
    }

    public void start() {
        if (!Utils.checkBluetooth(mBluetoothAdapter)) {
            Utils.requestUserBluetooth(beaconLocation);
            beaconLocation.stopScan();
        }
        else {
            scanLeDevice(true);
        }
    }

    public void stop() {
        scanLeDevice(false);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable && !mScanning) {
            Utils.toast(beaconLocation.getApplicationContext(), "Starting BLE scan...");

            mHandler.postDelayed(() -> {
                Utils.toast(beaconLocation.getApplicationContext(), "Stopping BLE scan...");

                mScanning = false;
                mBluetoothAdapter.stopLeScan(mLeScanCallback);

                beaconLocation.stopScan();
            }, scanPeriod);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
//            mBluetoothAdapter.startLeScan(uuids, mLeScanCallback);
        }
        else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

                    final int new_rssi = rssi;
                    if (rssi > signalStrength) {
                        mHandler.post(() -> beaconLocation.addDevice(device, new_rssi, scanRecord));
                    }
                }
            };



}
