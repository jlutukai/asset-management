package com.example.assetmanagement.model

data class Token(
    val token: String
)