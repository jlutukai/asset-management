package com.example.assetmanagement.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import kotlinx.android.synthetic.main.item_asset.view.*
import kotlinx.android.synthetic.main.item_asset.view.main_wrapper
import kotlinx.android.synthetic.main.item_nfc_asset.view.*

class SerialAdapterNfc (val context: Context, private val assets: List<String>) :
    RecyclerView.Adapter<SerialAdapterNfc.SerialNfcViewModel>() {
    var onItemClick: ((String) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SerialNfcViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_nfc_asset, parent, false)
        return SerialNfcViewModel(v)
    }

    override fun getItemCount(): Int {
        return assets.size
    }

    override fun onBindViewHolder(holder: SerialNfcViewModel, position: Int) {
        val asset = assets[position]
        holder.setData(asset, position)
    }
    inner class SerialNfcViewModel (itemView: View) : RecyclerView.ViewHolder(itemView){
        private var current: String? = null
        private var currentPos: Int = 0
        init {
            itemView.main_wrapper.setOnClickListener {
                current?.let {
                    onItemClick!!.invoke(it)
                }
            }
        }
        fun setData(asset: String, position: Int) {
            asset.let {
                itemView.serial_nfc.text = asset
            }
            current = asset
            currentPos = position
        }
    }
}