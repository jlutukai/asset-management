package com.example.assetmanagement.activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.example.assetmanagement.adapter.BatchesAdapter
import com.example.assetmanagement.model.AllAssetTypes
import com.example.assetmanagement.model.AllBatches
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.*
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_batches.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class Batches : AppCompatActivity() {
    private var batchList : MutableList<AllBatches> = arrayListOf()
    private var assetTypes : MutableList<AllAssetTypes> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_batches)
        setSupportActionBar(toolbar_b)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        val id: String? = intent.getStringExtra("id")
        rv_batches.layoutManager = GridLayoutManager(this, 2)
        rv_batches.addItemDecoration(SpacingItemDecoration(2, dpToPx(this, 8), true))
        rv_batches.setHasFixedSize(true)
        rv_batches.isNestedScrollingEnabled = false
        getBatchTypes()
        PushDownAnim.setPushDownAnimTo(add_batch).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(this, AddBatch::class.java).putExtra("id",id))
        }
    }

    private fun getBatchTypes() {
        if (isNetworkConnected(this)){
//            generalViewModel.getUpdatedAssetTypes()
            GlobalScope.launch(Dispatchers.Main) { batches()  }
        }else{
            showToast("Check Internet Connection")
        }
    }

    private suspend fun batches() {
      try{
          val response = ApiClient.webService.fetchAssets(getToken())
          if(response.isSuccessful){
              val res = response.body()
              res?.let {
                  var assetTypesNames : MutableList<String> = arrayListOf()
                  assetTypes.clear()
                  batchList.clear()
                  batchList.addAll(it.batches)
                  assetTypes.addAll(it.asset_types)
                  assetTypesNames.clear()
                  assetTypesNames.add("All Asset Types")
                  for(i in assetTypes) {
                      assetTypesNames.add(i.name)
                  }
                  val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
                      this,
                      android.R.layout.simple_spinner_item,
                      assetTypesNames as List<CharSequence>
                  )
                  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                  asset_type_spinner.adapter = adapter
                  asset_type_spinner.setTitle("Select Asset Types")
                  asset_type_spinner.onItemSelectedListener =
                      object : AdapterView.OnItemSelectedListener {
                          override fun onNothingSelected(p0: AdapterView<*>?) {
                          }

                          override fun onItemSelected(
                              p0: AdapterView<*>?,
                              p1: View?,
                              p2: Int,
                              p3: Long
                          ) {
                              progress.visibility = View.VISIBLE
                              val selectedItem = p0!!.getItemAtPosition(p2).toString()
                              if (selectedItem == "All Asset Types") {
                                  getBatches(null)
                              }
                              for (i in assetTypes) {
                                  if (i.name == selectedItem) {
                                      getBatches(i.id)
                                  }
                              }
                          }
                      }
              }
              }else{
              progress.visibility = View.GONE
              showToast("Failed : ${response.code()}")
          }
      }catch (e: IOException){
          progress.visibility = View.GONE
          showToast("Error : ${e.message}")
      }
    }


    private fun getBatches(id : Int?) {
        val batches : MutableList<AllBatches> = arrayListOf()
        if (id != null){
            batches.clear()
            for(i in batchList){
                if (i.asset_type_id == id){
                    batches.add(i)
                }
            }
            progress.visibility = View.GONE
            val batchesAdapter = BatchesAdapter(this, batches)
            rv_batches.adapter = batchesAdapter
            batchesAdapter.onItemClick = {
                val intent = Intent(this, Assets::class.java)
                intent.putExtra("sid", it.received_at.toString())
                intent.putExtra("id", it.id.toString() )
                startActivity(intent)
            }
        }else{
            batches.clear()
            batches.addAll(batchList)
            progress.visibility = View.GONE
            val batchesAdapter = BatchesAdapter(this, batches)
            rv_batches.adapter = batchesAdapter
            batchesAdapter.onItemClick = {
                val intent = Intent(this, Assets::class.java)
                intent.putExtra("sid", it.received_at.toString())
                intent.putExtra("id", it.id.toString() )
                startActivity(intent)
            }
        }

    }





    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

//try {
//    val response = ApiClient.webService.fetchAssets(getToken())
//    if (response.isSuccessful){
//        val allbatches = response.body()!!.batches
//        batchList = arrayListOf()
//        batchList.let {
//            progress.visibility = View.GONE
//            val batchesAdapter = BatchesAdapter(this, allbatches)
//            rv_batches.adapter = batchesAdapter
//            batchesAdapter.onItemClick = {
//                val intent = Intent(this, Assets::class.java)
//                intent.putExtra("sid", it.received_at.toString())
//                intent.putExtra("id", it.id.toString() )
//                startActivity(intent)
//            }
//        }
//    }else{
//        showToast("Failed : ${response.code()}")
//    }
//}catch (e: IOException){
//    showToast("Error : ${e.message}")
//}
