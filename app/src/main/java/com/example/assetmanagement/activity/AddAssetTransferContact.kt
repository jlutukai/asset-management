package com.example.assetmanagement.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.assetmanagement.R
import com.example.assetmanagement.fragments.FCToContact
import com.example.assetmanagement.fragments.FCToStation
import kotlinx.android.synthetic.main.activity_add_asset_transfer_contact.*

class AddAssetTransferContact : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_asset_transfer_contact)

        setSupportActionBar(toolbar_ac)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)


        if (savedInstanceState == null) {
//            bottom_nav.show(2)
            replaceFragment(FCToStation())

        }
        to_station.setOnClickListener {
            replaceFragment(FCToStation())
            to_contact.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
            to_contact.setTextColor(ContextCompat.getColor(this, android.R.color.black))
            to_station.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            to_station.setTextColor(ContextCompat.getColor(this, android.R.color.white))
        }
        to_contact.setOnClickListener {
            replaceFragment(FCToContact())
            to_station.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
            to_station.setTextColor(ContextCompat.getColor(this, android.R.color.black))
            to_contact.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
            to_contact.setTextColor(ContextCompat.getColor(this, android.R.color.white))
        }

    }
    private fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.viewpagerc, fragment)
        transaction.commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
