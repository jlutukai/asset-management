package com.example.assetmanagement.model

data class Property(
    val id : Int,
    val business_id : Int,
    val location_id : Int,
    val property_type_id : Int,
    val plot_number : String,
    val lr_number : String,
    val area : Double,
    val width : Double,
    val length : Double,
    val address : String,
    val land_use : String,
    val land_type : String,
    val land_rate : Double,
    val site_value : Double,
    val ground_rent : Double,
    val struck_value : Double,
    val name : String,
    val longitude :String?,
    val latitude : String?,
    val type : PropertyType,
    val location: Location
)