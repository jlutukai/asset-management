package com.example.assetmanagement.model

import androidx.room.Entity
import androidx.room.PrimaryKey

data class AllBatches(
    val id : Int,
    val asset_type_id : Int,
    val unit_id : Int,
    val batch_no : String,
    val image : String?,
    val description: String?,
    val received_at : String
)