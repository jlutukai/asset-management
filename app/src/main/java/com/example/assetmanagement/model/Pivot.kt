package com.example.assetmanagement.model

data class Pivot(
    val asset_movement_id : Int,
    val asset_id : Int,
    val status : Int
)