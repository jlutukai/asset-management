package com.example.assetmanagement.model

data class RegisterContactResponse(
    val message: String,
    val status_code  : Int,
    val result : Result
)