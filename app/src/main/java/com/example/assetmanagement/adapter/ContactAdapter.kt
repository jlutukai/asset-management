package com.example.assetmanagement.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.l4digital.fastscroll.FastScroller
import com.example.assetmanagement.activity.Contacts
import com.example.assetmanagement.model.Contact
import com.example.assetmanagement.model.ContactGroup
import kotlinx.android.synthetic.main.item_contact.view.*
import kotlinx.android.synthetic.main.item_contact_group.view.main_wrapper

class ContactAdapter(val context: Context, private val contacts: List<Contact>, private val groups: List<ContactGroup>) :
    RecyclerView.Adapter<ContactAdapter.ContactViewModel>(), FastScroller.SectionIndexer {
    private val co: Contacts = Contacts()
    var onItemClick: ((Contact) -> Unit)? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ContactViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_contact, parent, false)
        return ContactViewModel(v)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    @SuppressLint("DefaultLocale")
    override fun getSectionText(position: Int): CharSequence {
        return contacts[position].name[0].toString().capitalize()
    }

    override fun onBindViewHolder(holder: ContactViewModel, position: Int) {
        val contact = contacts[position]
        holder.setData(contact, position)
    }

    inner class ContactViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Contact? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper.setOnClickListener {
                current.let {
                    onItemClick!!.invoke(it!!)
                }

            }
        }

        fun setData(contact: Contact, position: Int) {
            contact.let {
                itemView.name.text = contact.name
                itemView.first.text = contact.name[0].toString().capitalize()
            }
            current = contact
            currentPos = position
        }

    }
}