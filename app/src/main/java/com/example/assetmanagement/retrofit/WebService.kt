package com.example.assetmanagement.retrofit

import com.example.assetmanagement.model.*
import retrofit2.Response
import retrofit2.http.*

interface WebService {
    @POST("login/")
    suspend fun loginUser(@Query("username") username: String, @Query("password") password: String): Response<Token>

    @GET("user")
    suspend fun getUserData(@Header("authorization") authorization: String): Response<UserData>

    @POST("contact/create")
    suspend fun addContact(
        @Header("authorization") authorization: String,
        @Query("name") name: String,
        @Query("email") email: String,
        @Query("mobile") phone: String,
        @Query("contact_id") idNo : String,
        @Query("physical_address") address : String?,
        @Query("customer_group_id") id: String?,
        @Query("supplier_business_name") biz: String,
        @Query("is_individual") ind :Int,
        @Query("birth_date") date : String,
        @Query("first_name") fname : String,
        @Query("last_name") lname : String,
        @Query("surname") sname : String,
        @Query("gender") gender : String,
        @Query("type") type : String,
        @Query("tax_number") tax : String?,
        @Query("city") city : String?,
        @Query("country") country : String?,
        @Query("landline") landline : String?,
        @Query("alternate_number") alt : String?,
        @Query("postal_address") post : String?,
        @Query("longitude") long: String,
        @Query("latitude") lat: String
    ): Response<ContactObj>

    @POST("contact/update/{id}")
    suspend fun updateContact(
        @Header("authorization") authorization: String,
        @Path("id") id: String,
        @Query("name") name: String,
        @Query("email") email: String?,
        @Query("mobile") phone: String?,
        @Query("supplier_business_name") biz: String?
    ): Response<ContactObj>

    //    @Multipart
    @POST("batch/create")
    suspend fun addBatch(
        @Header("authorization") authorization: String,
        @Query("description") desc: String?,
        @Query("batch_no") bno: String?,
        @Query("asset_type_id") assetid: String,
        @Query("unit_id") unitid: String,
        @Query("contact_id") contactid: String,
        @Query("has_warranty") warranty: Int,
        @Query("warranty_duration") duration: String,
        @Query("warranty_interval") interval: String,
        @Query("received_at") stationid: String,
        @Query("longitude") long: String,
        @Query("latitude") lat: String
//        @Query("description") desc : String,
//        @Part("image") image : File
    ): Response<AllBatches>

    @POST("batch/add_assets/{id}")
    suspend fun addAsset(
        @Header("authorization") authorization: String,
        @Path("id") id: String,
        @Query("received_at") sid: String,
        @Query("initial_value") value: String,
        @Query("serials") serial: String
    ): Response<Assetss>

    @POST("asset_transfer")
    suspend fun addMovement(
        @Header("authorization") authorization: String,
        @Query("from_station_id") fsid: String?,
        @Query("to_station_id") tsid: String?,
        @Query("created_by") uid: String,
        @Query("from_contact_id") fuid: String?,
        @Query("to_contact_id") tuid: String?,
        @Query("reason") reasons: String,
        @Query("reference") ref: String,
        @Query("longitude") long: String,
        @Query("latitude") lat: String
    ): Response<Movement>

    @POST("asset_transfer/change_status")
    suspend fun changeStatus(
        @Header("authorization") authorization: String,
        @Query("movement_id") mid: String,
        @Query("asset_id") aid: String,
        @Query("latitude") lat: String,
        @Query("longitude") long: String,
        @Query("status") status: String
    ): Response<Void>

    @POST("asset_transfer/add_assets")
    suspend fun addAssetsToMovement(
        @Header("authorization") authorization: String,
        @Query("movement_id") mid: Int,
        @Query("serials") serial: String
    ): Response<Void>//experiment - should return code  --

    @GET("asset_transfers")
    suspend fun fetchAssetTransfers(@Header("authorization") authorization: String): Response<AssetTranfers>

    @GET("asset_transfer_reasons")
    suspend fun fetchReasons(@Header("authorization") authorization: String): Response<Reasons>

    @GET("asset_types")
    suspend fun fetchAssets(@Header("authorization") authorization: String): Response<AllAssets>

    @GET("contact_groups")
    suspend fun fetchContactGroups(@Header("authorization") authorization: String): Response<ContactGroups>

    @GET("contacts")
    suspend fun fetchContacts(@Header("authorization") authorization: String): Response<Contacts>

    @GET("categories")
    suspend fun fetchCategories(@Header("authorization") authorization: String): Response<Categories>

    @GET("product/{id}")
    suspend fun fetchProducts(@Header("authorization") authorization: String, @Path("id") id: Int): Response<Products>

    @GET("property_types")
    suspend fun fetchPropertyTypes(@Header("authorization") authorization: String, @Header("accept") accept: String): Response<PropertyTypes>

    @GET("properties")
    suspend fun fetchProperties(@Header("authorization") authorization: String, @Header("accept") accept: String): Response<PropertyResponse>

    @POST("properties")
    suspend fun postproperty(
        @Header("authorization") authorization: String,
        @Header("accept") accept: String,
        @Query("business_id") bid: Int,
        @Query("location_id") lid: Int,
        @Query("property_type_id") pid: Int,
        @Query("plot_number") plotNumber: String,
        @Query("lr_number") lrNo: String,
        @Query("area") area: Double,
        @Query("width") width: Double,
        @Query("length") length: Double,
        @Query("address") address: String?,
        @Query("land_use") landUse: String?,
        @Query("land_type") landType: String,
        @Query("land_rate") landRate: Double,
        @Query("site_value") siteValue: Double,
        @Query("ground_rent") groundRent: Double,
        @Query("struck_value") struckValue: Double,
        @Query("name") name: String,
        @Query("longitude") longi: String?,
        @Query("latitude") lat: String?
    ): Response<Void>

    @POST("register/new")
    suspend fun registerNewUser(
        @Header("authorization") authorization: String,
        @Header("accept") accept: String,
        @Query("business_id") bid : Int,
        @Query("username") uname: String,
        @Query("first_name") fname: String,
        @Query("last_name") lname: String,
        @Query("phone") phone: String,
        @Query("password") password: String,
        @Query("longitude") longi: String?,
        @Query("latitude") lat: String?
    ): Response<RegisterContactResponse>

    @POST("property_types")
    suspend fun addPropertyType(
        @Header("authorization") authorization: String,
        @Header("accept") accept: String,
        @Query("business_id") bid: Int,
        @Query("name") name: String,
        @Query("description") desc: String?
    ) : Response<Void>

    @GET("farms")
    suspend fun getFarms(@Header("authorization") authorization: String) : Response<FarmResponse>

    @POST("farms")
    suspend fun postFarms(@Header("authorization") authorization: String,
                          @Query("business_id") bid : Int,
                          @Query("contact_id") cid : Int,
                          @Query("total_area") area : Double,
                          @Query("area_cultivated") areac : Double,
                          @Query("location_id") lid : Int,
                          @Query("address") address: String,
                          @Query("lr_number") lr : String,
                          @Query("zone_id") zid : Int,
                          @Query("longitude") longi: String?,
                          @Query("latitude") lat: String?
    ) : Response<Void>
}