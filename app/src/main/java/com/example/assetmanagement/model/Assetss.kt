package com.example.assetmanagement.model


data class Assetss(
    val id : Int,
    val serial : String,
    val rfid : String,
    val initial_value : String,
    val batch_id : Int,
    val current_station_id : Int?,
    val current_contact_id : Int?,
    val in_company : Int,
    val pivot : Pivot
)