package com.example.assetmanagement.model

data class AllAssets(
    val asset_types : List<AllAssetTypes>,
    val batches : List<AllBatches>,
    val assets : List<Assetss>,
    val stations : List<AllStations>,
    val suppliers : List<Contact>,
    val units : List<Units>,
    val vehicles : List<Vehicles>,
    val intervals : List<String>
)