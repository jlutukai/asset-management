package com.example.assetmanagement.model

data class PropertyType(
    val id : Int,
    val business_id : Int,
    val name : String,
    val description : String?,
    val deleted_at  : String?
)