package com.example.assetmanagement.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contacts")
data class Contact(
    @PrimaryKey
    val id: Int,
    val name: String,
    val email: String?,
    val mobile: String,
    val supplier_business_name: String?,
    val contact_id : String?,
    val customer_group_id : Int?,
    val is_individual: Int,
    val type : String?,
    val updated_at: String
) {
    constructor() : this(
        id = 0,
        name = "",
        email = "",
        mobile = "",
        supplier_business_name = "",
        contact_id = "",
        customer_group_id = 0,
        is_individual = 0,
        type = "",
        updated_at = ""
    )
}