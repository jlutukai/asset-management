package com.example.assetmanagement.utils

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.TypedValue
import android.widget.Toast
import kotlin.math.roundToInt


private lateinit var sessionManager: SharedPreferences
private lateinit var iD :String
private lateinit var siD :String


fun Context.storeToken(token: String) {
    sessionManager = this.getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
        ?: return
    with(sessionManager.edit()) {
        putString("tokenVal", token)
        apply()
    }
}

fun Context.getToken(): String {
    sessionManager = this.getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
    val token = sessionManager.getString("tokenVal", null)
    return token?.let {
        return@let token
    }.toString()
}
fun setId(id : String){
    iD  = id
}
fun setSid(id : String){
    siD = id
}

fun getid() :String{
    return iD
}
fun getsid() :String{
    return siD
}


fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun dpToPx(c: Context, dp: Int): Int {
    val r = c.resources
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics).roundToInt()
}

//network connection check
fun isNetworkConnected(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
    return activeNetwork?.isConnected == true
}