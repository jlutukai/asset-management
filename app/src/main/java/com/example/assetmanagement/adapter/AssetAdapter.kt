package com.example.assetmanagement.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.example.assetmanagement.model.Assetss
import kotlinx.android.synthetic.main.item_asset.view.*

class AssetAdapter (val context: Context, private val assets: List<Assetss>) :
    RecyclerView.Adapter<AssetAdapter.AssetsViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetsViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_asset, parent, false)
        return AssetsViewModel(v)
    }

    override fun getItemCount(): Int {
        return assets.size
    }

    override fun onBindViewHolder(holder: AssetsViewModel, position: Int) {
        val asset = assets[position]
        holder.setData(asset, position)
    }
    inner class AssetsViewModel (itemView: View) : RecyclerView.ViewHolder(itemView){
        private var current: Assetss? = null
        private var currentPos: Int = 0
        init {
            itemView.main_wrapper.setOnClickListener {
            }
        }
        fun setData(asset: Assetss, position: Int) {
            asset.let {
                itemView.serial.text = asset.serial
                itemView.rfid.text = asset.rfid
                itemView.value.text = asset.initial_value
            }
            current = asset
            currentPos = position
        }
    }
}