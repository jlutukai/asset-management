package com.example.assetmanagement.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "sales")
data class SalesLocal(
    @PrimaryKey
    @ColumnInfo(name = "id") val id: Int,
    val name: String,
    val default_purchase_price: Double,
    val default_sell_price: Double,
    val quantity: Int
) {
    constructor() : this(0, "", 0.0, 0.0, 0)
}