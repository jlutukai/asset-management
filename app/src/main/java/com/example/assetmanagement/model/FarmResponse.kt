package com.example.assetmanagement.model

data class FarmResponse(
    val location_types : List<LocationType>,
    val locations : List<Location>,
    val zones : List<Zone>,
    val farms : List<Farm>
)