package com.example.assetmanagement.activity

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.example.assetmanagement.adapter.AssetTypesAdapter
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.room.viewModel.GeneralViewModel
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.isNetworkConnected
import com.example.assetmanagement.utils.showToast
import kotlinx.android.synthetic.main.activity_asset_types.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class AssetTypes : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var loading : Dialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asset_types)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        loading = Dialog(this)
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loading.setCancelable(false)
        loading.setContentView(R.layout.loading_dialog)
        loading.show()
        rv_asset_types.layoutManager = LinearLayoutManager(this)
        rv_asset_types.isNestedScrollingEnabled = true

//        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getAssetTypes()
    }

    private fun getAssetTypes() {
            GlobalScope.launch(Dispatchers.Main) { assetTypes()  }
//
    }

    private suspend fun assetTypes() {
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful){
                val assets = response.body()!!.asset_types
                assets.let {
                    loading.dismiss()
                    progress.visibility = View.GONE
                    val assetTypesAdapter = AssetTypesAdapter(this, assets)
                    rv_asset_types.adapter = assetTypesAdapter
                }
            }else{
                loading.dismiss()
                showToast("Failed : ${response.code()}")
            }
        }catch (e:IOException){
            loading.dismiss()
            showToast("Error : ${e.message}")
        }
loading.dismiss()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
