package com.example.assetmanagement.activity

import android.app.Dialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.example.assetmanagement.adapter.SerialAdapterNfc
import com.example.assetmanagement.model.AllStations
import com.example.assetmanagement.model.Assetss
import com.example.assetmanagement.model.Message
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.getid
import com.example.assetmanagement.utils.getsid
import com.example.assetmanagement.utils.showToast
import com.github.nikartm.button.FitButton
import com.google.gson.Gson
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_capture_nfc.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CaptureNfc : AppCompatActivity() {
    private var nfcAdapter: NfcAdapter? = null
    private lateinit var pendingIntent: PendingIntent
    private lateinit var adapter: SerialAdapterNfc
    private var serials: MutableList<String> = arrayListOf()
    private  var stations : MutableList<AllStations> = arrayListOf()
    private  var assets : MutableList<Assetss> = arrayListOf()
    private  var allassets : ArrayList<String> = arrayListOf()
    private  var allassetsinc : ArrayList<String> = arrayListOf()
    private  var allassetsins : ArrayList<String> = arrayListOf()
    private  var type : String? = null
    private  var stid: String? = null
    private var cid : String? = null
    private var id : String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture_nfc)
        setSupportActionBar(toolbar_nfc)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        type = intent.getStringExtra("type")
        stid = intent.getStringExtra("stid")
        cid = intent.getStringExtra("cid")
        id  = intent.getStringExtra("id")
        getData()
        rv_serials_nfc.layoutManager = LinearLayoutManager(this)
        rv_serials_nfc.isNestedScrollingEnabled = false
        adapter = SerialAdapterNfc(this, serials)
        rv_serials_nfc.adapter = adapter
        adapter.onItemClick = {
            serials.remove(it)
            adapter.notifyDataSetChanged()
            submit_nfc.isEnabled = serials.isNotEmpty()
        }
        val manager = getSystemService(Context.NFC_SERVICE) as NfcManager
        nfcAdapter = manager.defaultAdapter
        if (nfcAdapter != null && nfcAdapter!!.isEnabled) {
            showToast("Device is NFC enabled")
            
        }else{
            showToast("Device does not support nfc")
            finish()
            return
        }

        pendingIntent = PendingIntent.getActivity(
            this,
            0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
            0
        )

        PushDownAnim.setPushDownAnimTo(submit_nfc).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            doInBg()
            }



    }

    private fun doInBg() {
        if (type == "addtobatch") GlobalScope.launch(Dispatchers.Main) { addToBatch() }
        if (type == "addToMovement" ) GlobalScope.launch(Dispatchers.Main) { addToMovement() }

    }

    private suspend fun addToMovement() {
        try {
            val response = ApiClient.webService.addAssetsToMovement(getToken(),id!!.toInt(),serials.joinToString(","))
            if (response.code() == 200){
                showToast("Assets Captured Successfully")
                startActivity(Intent(this, AssetManagement::class.java))
                finish()
            }
        }catch (e:IOException){
            showToast("Error : ${e.message}")
        }
    }

    private suspend fun addToBatch() {
        val id: String = getid()
        val sid: String = getsid()
        val initial = "2000"
        try {
            val response = ApiClient.webService.addAsset(
                getToken(),
                id,
                sid,
                initial,
                serials.joinToString(",")
            )
            if (response.code() == 200) {
                showToast("Assets Captured Successfully")
                startActivity(Intent(this, Assets::class.java).putExtra("id", id).putExtra("sid", sid))
                finish()
                Log.d("bjdsbjdbsdf", "success")
            } else {
                val gson = Gson()
                val errorResponse = gson.fromJson<Message>(
                    response.errorBody()!!.charStream(),
                    Message::class.java
                )
                val msg = errorResponse.message
                showToast(msg)
            }
        } catch (e: IOException) {
            showToast("Error : ${e.message}")
        }
    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getAllAssets() }
    }

    private suspend fun getAllAssets() {
        allassets.clear()
        allassetsins.clear()
        allassetsinc.clear()
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful) {
                stations.clear()
                stations.addAll(response.body()!!.stations)
                assets.clear()
                assets.addAll(response.body()!!.assets)
            }
        } catch (e: IOException) {
            showToast("Error : ${e.message}")
        }
        for (i in assets){
            allassetsins.add(i.serial)
        }
        if(!cid.isNullOrEmpty()) {
            for (i in assets) {
                if (i.current_contact_id.toString() == cid) {
                    allassets.add(i.serial)
                }
            }
        }
        if(!stid.isNullOrEmpty()) {
            for (i in assets) {
                if (cid.isNullOrEmpty()) {
                    if (i.current_station_id.toString() == stid) {
                        allassets.add(i.serial)
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            if (!nfcAdapter!!.isEnabled)
                showWirelessSettings()
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
    }

    private fun showWirelessSettings() {
        showToast("You need to enable NFC")
        val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
        startActivity(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            resolveIntent(intent)
        }
    }

    private fun resolveIntent(intent: Intent) {
        val action = intent.action
        if (NfcAdapter.ACTION_TAG_DISCOVERED == action || NfcAdapter.ACTION_TECH_DISCOVERED == action || NfcAdapter.ACTION_NDEF_DISCOVERED == action) {
                val id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
                var hexdump = String()
                for (i in id.indices) {
                    var x =
                        Integer.toHexString(id[i].toInt() and 0xff)
                    if (x.length == 1) {
                        x = "0$x"
                    }
                    hexdump += "$x "
                }
                val strn = hexdump.replace("\\s".toRegex(), "")
            if (type == "addtobatch"){
                if (!serials.contains(strn) && !allassetsins.contains(strn)){
                    showSerialDialog(strn,"to batch")
                }else{
                    showToast("$strn already exists")
                }
            }
            if (type == "addToMovement"){
                if (!serials.contains(strn) && allassets.contains(strn)){
                    showSerialDialog(strn, "to transfer")
                }else if (!allassets.contains(strn)){
                    showToast("$strn does not exist")
                }else{
                    showToast("$strn already exist")
                }
            }


        }
    }

    private fun showSerialDialog(hexdump: String, ty :String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_serial)
        val txt = dialog.findViewById<TextView>(R.id.content_serial)
        txt.text = "Are you sure you want to \n add $hexdump $ty?"
        val close = dialog.findViewById<FitButton>(R.id.close_dialog_serial)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val confirm = dialog.findViewById<FitButton>(R.id.confirm_serial)
        confirm.setOnClickListener {
            serials.add(hexdump)
            adapter.notifyDataSetChanged()
            submit_nfc.isEnabled = serials.isNotEmpty()
            dialog.dismiss()
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
