package com.example.assetmanagement.room.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.assetmanagement.model.*
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.room.AppDatabase
import com.example.assetmanagement.room.RoomDbInit
import com.example.assetmanagement.utils.getToken
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class GeneralRepository {

    private val db: AppDatabase = RoomDbInit.database

    //contacts
    fun apiContacts(application: Application){
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchContacts(application.getToken())
                if (response.isSuccessful){
                    val contact = response.body()
                    contact?.let {
                        db.generalDao().nukeContacts()
                        db.generalDao().saveContactLocal(it.contacts)
                    }
                }
            }catch (e:IOException){
                //errors
            }
        }
    }
    fun localContacts() : LiveData<List<Contact>>{
        return db.generalDao().getLocalContacts()
    }


    //contact groups
    fun apiContactGroups(application: Application){
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchContactGroups(application.getToken())
                if (response.code() == 200){
                    val contactGroup = response.body()
                    contactGroup?.let {
                        db.generalDao().nukeContactGroups()
                        db.generalDao().saveContactGroupsLocal(it.contact_groups)
                    }
                }
            }catch (e : IOException){
                //errors
            }
        }
    }
    fun localContactGroups() : LiveData<List<ContactGroup>>{
        return db.generalDao().getLocalContactGroups()
    }
    //categories
    fun apiCategories(application: Application) {
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchCategories(application.getToken())
                if (response.code() == 200) {
                    val category = response.body()
                    category?.let {
                        db.generalDao().nukeCategories()
                        db.generalDao().saveCategoriesLocal( it.categories)
                    }
                }
            } catch (e: IOException) {
                //errors e.g. no internet
            }
        }

    }

    fun localCategories(): LiveData<List<Category>> {
        return db.generalDao().getLocalCategories()
    }

    //products
    fun apiProducts(application: Application) {
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchProducts(application.getToken(), 14)
                if (response.code() == 200) {
                    val product = response.body()
                    product?.let {
                        db.generalDao().nukeProducts()
                        db.generalDao().saveProductsLocal(it.products)
                    }
                }
            } catch (e: IOException) {
                //errors e.g. no internet
            }
        }

    }

    fun localProducts(): LiveData<List<Product>> {
        return db.generalDao().getLocalProducts()
    }

    //ITEMS-SALES
    fun roomDbItems(salesLocal: SalesLocal) {
        GlobalScope.launch {
            val item = db.generalDao().checkItemExists(salesLocal.id)
            if (item != null) {
                db.generalDao().updateItem(quantity = item.quantity + 1, pid = salesLocal.id)
                Log.d("add-items", item.toString())
            } else {
                db.generalDao().addItem(salesLocal)
                Log.d("add-items",  "added new item\n$salesLocal")
            }
        }
    }

    // Property types
//    fun getPropertyTypes(application: Application) : LiveData<List<PropertyType>>{
//        val propertyTypes : MutableList<PropertyType> = arrayListOf()
//        GlobalScope.launch {
//        try {
//            val response = ApiClient.webService.fetchPropertyTypes(application.getToken())
//            if (response.isSuccessful){
//                val pro = response.body()!!.types
//                pro.let {
//                    propertyTypes.addAll(it)
//                }
//            }
//        }catch (e:IOException){
//
//        }
//        }
//        return propertyTypes
//    }
}