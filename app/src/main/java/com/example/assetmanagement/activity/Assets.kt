package com.example.assetmanagement.activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.example.assetmanagement.adapter.AssetAdapter
import com.example.assetmanagement.model.AllBatches
import com.example.assetmanagement.model.Assetss
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.*
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_assets.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class Assets : AppCompatActivity() {
    private lateinit var assetList: MutableList<Assetss>
    private lateinit var assets: List<Assetss>
    private lateinit var serials : String
    private lateinit var rfids : String
    private  var stationid: String = ""
    private lateinit var values : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assets)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)

        val id: String? = intent.getStringExtra("id")
        val sid : String? = intent.getStringExtra("sid")
        setId(id!!)
        setSid(sid!!)
        rv_assets.layoutManager = LinearLayoutManager(this)
        rv_assets.isNestedScrollingEnabled = false
        getLists()

        PushDownAnim.setPushDownAnimTo(add_asset).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
//            showToast("$id $sid")
            values = id
            serials = sid
            showOptionsDialog()
        }
    }

    private fun showOptionsDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_options_tech)
        val close = dialog.findViewById<ImageView>(R.id.close_dialog_tech)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val nfc = dialog.findViewById<TextView>(R.id.nfc)
        PushDownAnim.setPushDownAnimTo(nfc).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(this, CaptureNfc::class.java).putExtra("type" , "addtobatch")
                .putExtra("stid", stationid))
            dialog.dismiss()
        }
        val ble = dialog.findViewById<TextView>(R.id.beacons)
        PushDownAnim.setPushDownAnimTo(ble).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(this, CaptureBeacons::class.java).putExtra("type" , "addtobatch")
                .putExtra("stid", stationid))
            dialog.dismiss()
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }


    private fun getLists() {
        if (isNetworkConnected(this)) {
            GlobalScope.launch(Dispatchers.Main) { lists() }
        } else {
            progress.visibility = View.GONE
            showToast("Check Internet Connection")
        }
    }

    private suspend fun lists() {
        val id: String? = intent.getStringExtra("id")
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful) {
                assets = response.body()!!.assets
                assetList = arrayListOf()
                for (i in assets) {
                    if (i.batch_id == id!!.toInt()) {
                        assetList.add(i)
                        stationid = i.current_station_id.toString()
                    }
                }
                if (assetList.size == 0){
                    showToast("No Assets Available currently...")
                }
                assetList.let {
                    progress.visibility = View.GONE
                    val assetAdapter = AssetAdapter(this, assetList)
                    rv_assets.adapter = assetAdapter
                }
            }

        } catch (e: IOException) {

        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
