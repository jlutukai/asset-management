package com.example.assetmanagement.model

data class Zone(
    val id: Int,
    val business_id: Int,
    val name: String,
    val created_at: String,
    val updated_at: String
)