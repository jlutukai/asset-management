package com.example.assetmanagement.retrofit

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

//singleton - only called once for every activity
const val baseUrl = "http://165.90.23.196/api/"
//const val baseUrl = "https://jsonplaceholder.typicode.com/"

object ApiClient {
    val webService: WebService by lazy {
                val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client: OkHttpClient = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60,TimeUnit.SECONDS)
            .addInterceptor(logging)
            .build()
        Log.d("APiAccess", "creating retrofit client")
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
//            .addCallAdapterFactory() -- deprecated
            .build()

        //create retrofit client
        return@lazy retrofit.create(WebService::class.java)
    }
}