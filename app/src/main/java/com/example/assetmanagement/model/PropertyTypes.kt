package com.example.assetmanagement.model

import com.example.assetmanagement.model.PropertyType

data class PropertyTypes(
    val types : List<PropertyType>
)