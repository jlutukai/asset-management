package com.example.assetmanagement.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.example.assetmanagement.activity.Assets
import com.example.assetmanagement.model.AllBatches
import com.example.assetmanagement.utils.showToast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_batch.view.*
import kotlinx.android.synthetic.main.item_contact_group.view.*
import kotlinx.android.synthetic.main.item_contact_group.view.cust_group
import kotlinx.android.synthetic.main.item_contact_group.view.main_wrapper

class BatchesAdapter (val context: Context, private val batches: List<AllBatches>) :
    RecyclerView.Adapter<BatchesAdapter.BatchesViewModel>() {
    var onItemClick: ((AllBatches) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BatchesViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_batch, parent, false)
        return BatchesViewModel(v)
    }

    override fun getItemCount(): Int {
        return batches.size
    }

    override fun onBindViewHolder(holder: BatchesViewModel, position: Int) {
        val batch = batches[position]
        holder.setData(batch, position)
    }
    inner class BatchesViewModel(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var current: AllBatches? = null
        private var currentPos: Int = 0
        init {
            itemView.main_wrapper.setOnClickListener {
                current?.let {
                    onItemClick!!.invoke(it)
                }
            }
        }

        fun setData(batch: AllBatches, position: Int) {
            batch.let {
                itemView.cust_group.text = batch.batch_no
                itemView.description.text = batch.description
                if (batch.image != null) {
                    Picasso.get().load("http://165.90.23.196/uploads/img/${batch.image}")
                        .into(itemView.image_batch)
                }
            }
            current = batch
            currentPos = position
        }

    }
}