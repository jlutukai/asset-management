package com.example.assetmanagement.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products")
data class Product(
    @PrimaryKey val id: Int,
    val name: String,
    val default_purchase_price: Double,
    val default_sell_price: Double
) {
    constructor() : this(0, "", 0.0, 0.0)
}