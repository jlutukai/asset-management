package com.example.assetmanagement.model

data class Reasons(
    val transfer_reasons : List<TransferReasons>
)