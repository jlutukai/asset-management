package com.example.assetmanagement.model

data class UserDetails(

    val id : Int,
    val surname : String,
    val first_name : String,
    val last_name : String,
    val username : String,
    val is_customer : String,
    val business_id : Int,
    val email : String?
)