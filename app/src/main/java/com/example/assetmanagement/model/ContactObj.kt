package com.example.assetmanagement.model

data class ContactObj(
    val name: String,
    val email: String,
    val mobile: String,
    val supplier_business_name : String?,
    val customer_group_id: String
//    val business_id: Int?,
//    val type: String?,
//    val contact_id: String?,
//    val created_by: Int?,
//    val updated_at: String?,
//    val created_at: String?,
//    val id: Int?

)