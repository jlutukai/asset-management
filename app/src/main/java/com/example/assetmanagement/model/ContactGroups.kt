package com.example.assetmanagement.model

import com.example.assetmanagement.model.ContactGroup

data class ContactGroups(
    val contact_groups : List<ContactGroup>
)