package com.example.assetmanagement.model


data class AllStations(
    val id : Int,
    val name : String,
    val landmark : String,
    val business_id : Int,
    val pivot : StationPivot
)
