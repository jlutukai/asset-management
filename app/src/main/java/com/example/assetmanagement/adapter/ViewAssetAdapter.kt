package com.example.assetmanagement.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.example.assetmanagement.model.Assetss
import com.example.assetmanagement.model.Contact
import com.example.assetmanagement.model.Movements
import com.example.assetmanagement.model.User
import com.example.assetmanagement.utils.showToast
import kotlinx.android.synthetic.main.item_asset_status.view.*

class ViewAssetAdapter(
    val context: Context,
    private var assets: MutableList<Assetss>,
    private val statuses: List<String>,
    private val user: User,
    private val movement: Movements,
    private val contact: Contact?,
    private val stids: MutableList<String>
) :
    RecyclerView.Adapter<ViewAssetAdapter.ViewAssetsViewModel>() {
    var onItemClickRecieve: ((Assetss) -> Unit)? = null
    var onItemClickReject: ((Assetss) -> Unit)? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewAssetsViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_asset_status, parent, false)
        return ViewAssetsViewModel(v)
    }

    override fun getItemCount(): Int {
        return assets.size
    }

    override fun onBindViewHolder(holder: ViewAssetsViewModel, position: Int) {
        val asset = assets[position]
        holder.setData(asset, position)
    }

    fun update(updatedassets: List<Assetss>) {
        assets.clear()
        assets = updatedassets as MutableList<Assetss>
    }

    inner class ViewAssetsViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Assetss? = null
        private var currentPos: Int = 0

        init {
            itemView.recieve.setOnClickListener {
                current?.let {
                    onItemClickRecieve!!.invoke(it)
                }
            }
            itemView.reject.setOnClickListener {
                current?.let {
                    onItemClickReject!!.invoke(it)
                }
            }
        }

        fun setData(asset: Assetss, position: Int) {
            asset.let {
                itemView.serial.text = asset.serial
                val s: String = statuses[asset.pivot.status]
                itemView.status.text = s
//                if (user.stations.isNotEmpty()) {
                    if (asset.pivot.status == 1 ) {
                        itemView.recieve.visibility = View.VISIBLE
                        itemView.reject.visibility = View.VISIBLE
                    }
//                }
//                if (contact != null) {
//                    if (asset.pivot.status == 1 && movement.to_contact_id == contact.id) {
//                        itemView.recieve.visibility = View.VISIBLE
//                        itemView.reject.visibility = View.VISIBLE
//                    }

                    current = asset
                    currentPos = position
                }
            }
        }

    }


