package com.example.assetmanagement.activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.example.assetmanagement.R
import com.example.assetmanagement.model.AllStations
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_asset_management.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class AssetManagement : AppCompatActivity() {
    private lateinit var contactid : String
    private lateinit var stationid : String
    private lateinit var iscustomer : String
    private lateinit var stations : List<AllStations>
    private lateinit var stationNames : MutableList<String>
    private lateinit var loading : Dialog
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asset_management)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        loading = Dialog(this)
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loading.setCancelable(false)
        loading.setContentView(R.layout.loading_dialog)
        loading.show()
        getData()
        PushDownAnim.setPushDownAnimTo(assets_all).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(this@AssetManagement, Batches::class.java))
        }
        PushDownAnim.setPushDownAnimTo(asset_location).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {

            val station: String = stations_am.selectedItem.toString()
            for (i in stations) {
                if (station == i.name) {
                    stationid = i.id.toString()
                }
            }

            startActivity(Intent(this@AssetManagement, BeaconLocation::class.java).putExtra("id",stationid))
        }


    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) {getUsersData()}
    }

    private suspend fun getUsersData() {
        contactid = ""
        try {
            val response = ApiClient.webService.getUserData(getToken())
            if (response.isSuccessful){
//                if (response.body()!!.user.is_customer == 0) {
                    stations = response.body()!!.user.stations
                    val uid =response.body()!!.user.id
                    iscustomer = response.body()!!.user.is_customer.toString()
                        if (stations.isNotEmpty()){
//                            stations_am.visibility = View.VISIBLE
                            stationNames = arrayListOf()
                    for (i in stations) {
                          stationNames.add(i.name  )
                    }
                            if (stationNames.isNotEmpty()){
                                stations_am.visibility = View.VISIBLE
                                assets_all.visibility = View.VISIBLE
                            }
                            val ac = ArrayAdapter(this, android.R.layout.simple_spinner_item, stationNames)
                            ac.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            with(stations_am) {
                                adapter = ac
                                setSelection(0, false)
                                prompt = "Select Station"
                                gravity = Gravity.START
                            }



                    }
                            if (response.body()!!.contact != null) {
                                cuid.text = response.body()!!.user.first_name
                                contactid = response.body()!!.contact!!.id.toString()
                                PushDownAnim.setPushDownAnimTo(asset_transfers)
                                    .setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
                                        startActivity(
                                            Intent(
                                                this@AssetManagement,
                                                AllAssetTransfers::class.java
                                            ).putExtra("id", contactid).putExtra("type", "conta")
                                        )
                                    }
                            }


                    if (stations.isNotEmpty()){
                    PushDownAnim.setPushDownAnimTo(asset_transfers).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
                        startActivity(Intent(this@AssetManagement, AllAssetTransfers::class.java).
                            putExtra("type", "biz"))
                    }}

//                    if (!contactid.isNullOrBlank()) {

            }
        }catch (e: IOException){
            showToast("Error : ${e.message}")
            loading.dismiss()
        }
        loading.dismiss()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
