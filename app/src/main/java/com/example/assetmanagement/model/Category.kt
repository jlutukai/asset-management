package com.example.assetmanagement.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categories")
data class Category(
    @PrimaryKey
    val id:Int,
    val name:String,
//    @Ignore val business_id:Int,
//    val short_code:Int,
//    @Ignore val parent_id:Int,
//    @Ignore val created_by:Int,
    val updated_at:String
//    @Ignore val is_unstructured:Int,
//    @Ignore val allow_self:Int
){constructor(): this(id = 0,name = "",updated_at = "")}