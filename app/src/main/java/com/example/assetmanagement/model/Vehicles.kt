package com.example.assetmanagement.model

data class Vehicles(
    val id : Int,
    val business_id : Int,
    val driver_id : Int,
    val transporter_id  :Int,
    val reg_no : String,
    val active : Int,
    val capacity : Int,
    val type : Type,
    val unit : Units,
    val driver : User,
    val transporter: Contact

)