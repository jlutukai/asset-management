package com.example.assetmanagement.fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.Fragment
import com.example.assetmanagement.R
import com.example.assetmanagement.activity.AssetManagement
import com.example.assetmanagement.activity.Contacts
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException


class HomeFragment : Fragment() {
    private lateinit var loading : Dialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loading = Dialog(context!!)
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loading.setCancelable(false)
        loading.setContentView(R.layout.loading_dialog)
        loading.show()
        PushDownAnim.setPushDownAnimTo(contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                startActivity(Intent(context, Contacts::class.java))
            }
        PushDownAnim.setPushDownAnimTo(asset).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                startActivity(Intent(context, AssetManagement::class.java))
            }

        getData()
    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getUsersData()  }
    }

    private suspend fun getUsersData() {
        try {
            val response = ApiClient.webService.getUserData(context!!.getToken())
            if (response.isSuccessful){
//                username.text = response.body()!!.user.username
                company.text = response.body()!!.user.first_name
            }
        }catch (e:IOException){
            context!!.showToast("Error : ${e.message}")
            loading.dismiss()
        }
        loading.dismiss()
    }

}