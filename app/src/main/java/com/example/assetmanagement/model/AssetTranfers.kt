package com.example.assetmanagement.model

data class AssetTranfers(
    val movements : List<Movements>,
    val movement_status : List<String>,
    val asset_movement_status : List<String>,
    val contacts : List<Contact>
)