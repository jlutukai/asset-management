package com.example.assetmanagement.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.l4digital.fastscroll.FastScrollRecyclerView
import com.example.assetmanagement.adapter.ContactAdapter
import com.example.assetmanagement.model.Contact
import com.example.assetmanagement.model.ContactGroup
import com.example.assetmanagement.model.Farm
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.room.viewModel.GeneralViewModel
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.isNetworkConnected
import com.example.assetmanagement.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_contacts.*
import kotlinx.android.synthetic.main.activity_contacts.progress
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.cachapa.expandablelayout.ExpandableLayout
import java.io.IOException
import java.util.*
import kotlin.Comparator


class Contacts : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var contactList: MutableList<Contact>
    private var contactGroupList: MutableList<ContactGroup> = arrayListOf()
    private var contactGroupNames: MutableList<String> = arrayListOf()
    private lateinit var recyclerView: FastScrollRecyclerView
    var search  : MutableList<Contact> = arrayListOf()
    var allFarms: MutableList<Farm> = arrayListOf()
    private lateinit var contact: Contact
    private lateinit var farm: Farm
    @SuppressLint("DefaultLocale")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)


        recyclerView = findViewById(R.id.rv_contacts)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.isNestedScrollingEnabled = false

        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getContacts()
//        var bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)

        PushDownAnim.setPushDownAnimTo(add).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                //            val addContact = AddContact()
//            addContact.show(supportFragmentManager,addContact.tag)
                startActivity(
                    Intent(this@Contacts, AddContact::class.java)
                )
            }
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        val stations: List<String> = listOf()
        menuInflater.inflate(R.menu.search, menu)
        val searchItem = menu!!.findItem(R.id.action_search)
        if (searchItem != null){
            val searchView = searchItem.actionView as SearchView
            val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            editText.hint = "Search here..."

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if (newText!!.isNotEmpty()){
                        search.clear()
                        val  char = newText.toLowerCase(Locale.getDefault())
                        for (i in contactList){
//                            if(i.from_station_id != null && i.to_station_id != null )
                            if (i.name.toLowerCase().contains(char)
                            ){
                                search.add(i)
                            }
                        }
                        recyclerView.adapter!!.notifyDataSetChanged()
                    }else{
                        search.clear()
                        search.addAll(contactList)
                        recyclerView.adapter!!.notifyDataSetChanged()
                    }

                    return true
                }

            })
        }

        return super.onCreateOptionsMenu(menu)
    }


    @SuppressLint("DefaultLocale")
    private fun getContacts() {
        if (isNetworkConnected(this)) {
            generalViewModel.getUpdatedContacts()
        } else {
            showToast("From Room")
        }
        generalViewModel.getLocalContactGroups()
            .observe(this@Contacts, Observer { contactGroups ->
                if (contactGroups.isNotEmpty()) {
                    progress.visibility = View.GONE
                    contactGroupList.addAll(contactGroups)
                }
            })
        generalViewModel.getLocalContacts().observe(this@Contacts, Observer { contacts ->
            if (contacts.isNotEmpty()) {
                contactList = arrayListOf()
                contactList.addAll(contacts)
                progress.visibility = View.GONE
                contactList.sortWith(Comparator { lhs, rhs -> lhs.name.toLowerCase().compareTo(rhs.name.toLowerCase()) })
                search.clear()
                search.addAll(contactList)
                val contactAdapter = ContactAdapter(this, search, contactGroupList)
                recyclerView.adapter = contactAdapter
                contactAdapter.onItemClick= {
                    contact = it
                    showContactDialog(contact)
                }
            }

        })

    }

    private fun showContactDialog(contact: Contact) {
        var farms: MutableList<Farm> = arrayListOf()
        val name: String = contact.name
        val id: String = contact.id.toString()
        val email: String? = contact.email
        val number: String = contact.mobile
        val biz: String? = contact.supplier_business_name
        val gid : Int? = contact.customer_group_id
        val type : String? = contact.type
        val isIndividual : Int  = contact.is_individual
        var txt = ""
        if (isIndividual == 1){
            txt = "Individual"
        }
        if (isIndividual == 0){
            txt = "Company"
        }
        var group : String = ""
        if (gid != null){
            for (i in contactGroupList){
                if (i.id == gid){
                    group = i.name
                }
            }
        }else{
            group = "Customer"
        }
        for (i in allFarms){
            if (contact.id == i.contact_id){
                farms.add(i)
            }
        }
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.contact_dialog)
        val nameC: TextView = dialog.findViewById(R.id.name_contact)
        nameC.text = "$name\n($txt)"
        val groupings : TextView = dialog.findViewById(R.id.customer_group)
        groupings.text = group
        val emailC: TextView = dialog.findViewById(R.id.email_contact)
        if (!email.isNullOrBlank()) {
            emailC.visibility = View.VISIBLE
            emailC.text = email
        }
        val phone: TextView = dialog.findViewById(R.id.mobile_contacts)
        phone.text = number
        val business: TextView = dialog.findViewById(R.id.biz_contacts)
        if (!biz.isNullOrBlank()) {
            business.visibility = View.VISIBLE
            business.text = biz
        }
        val close: ImageView = dialog.findViewById(R.id.close_dialog_contact)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val edit: TextView = dialog.findViewById(R.id.edit_dialog_contact)
        edit.setOnClickListener {
            val intent = Intent(this, EditContact::class.java)
            intent.putExtra("id", id)
            intent.putExtra("name", name)
            intent.putExtra("email", email)
            intent.putExtra("number", number)
            intent.putExtra("biz", biz)
            this.startActivity(intent)
            dialog.dismiss()
        }

        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }



    fun showDetails(name: String) {
        showToast(name)
//        val dialog = Dialog(this)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setCancelable(true)
//        dialog.setContentView(R.layout.contact_dialog)
//        val body : TextView = dialog.findViewById(R.id.name_contact)
//        body.text = name
//        dialog.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
