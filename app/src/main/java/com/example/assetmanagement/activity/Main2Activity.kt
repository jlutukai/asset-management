package com.example.assetmanagement.activity

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration
import com.example.assetmanagement.R
import com.google.android.material.navigation.NavigationView
import com.example.assetmanagement.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.app_bar_main2.*

class Main2Activity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


//        bottom_nav.add(MeowBottomNavigation.Model(1, R.drawable.mall))
//        bottom_nav.add(MeowBottomNavigation.Model(2, R.drawable.ic_dashboard_black_24dp))
//        bottom_nav.add(MeowBottomNavigation.Model(3, R.drawable.ic_shopping_cart_black_24dp))

        if (savedInstanceState == null) {
//            bottom_nav.show(2)
            replaceFragment(HomeFragment())
            toolbar.title = "DashBoard"
        }
//        bottom_nav.setOnClickMenuListener {
//            when (it.id) {
//                1 -> {
//                    replaceFragment(Pos())
//                    toolbar.title = "POS"
//                }
//                2 -> {
//                    replaceFragment(HomeFragment())
//                    toolbar.title = "Home"
//                }
//                3 -> {
//                    replaceFragment(Cart())
//                    toolbar.title = "Cart"
//                }
//            }
//        }


        val drawer: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawer,
            toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()


        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)
//        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        appBarConfiguration = AppBarConfiguration(
//            setOf(
//                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
//                R.id.nav_tools, R.id.nav_share, R.id.nav_send
//            ), drawerLayout
//        )
//        setupActionBarWithNavController(navController, appBarConfiguration)
//        navView.setupWithNavController(navController)
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame, fragment)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main2, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.logout -> {
                logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun logout() {
        startActivity(Intent(this, Login::class.java))
        finish()
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.nav_home -> {
                replaceFragment(HomeFragment())
                toolbar.title = "DashBoard"
            }
//            R.id.nav_pos -> {
//                replaceFragment(Pos())
//                toolbar.title = "POS"
//            }
//            R.id.nav_cart -> {
//                replaceFragment(Cart())
//                toolbar.title = "Cart"
//            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


//    override fun onSupportNavigateUp(): Boolean {
//        val navController = findNavController(R.id.nav_host_fragment)
//        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
//    }
}
