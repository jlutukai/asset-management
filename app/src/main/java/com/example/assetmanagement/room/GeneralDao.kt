package com.example.assetmanagement.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.assetmanagement.model.*

@Dao
interface GeneralDao {
//    // Assets Types
//    @Insert(onConflict = REPLACE)
//    fun saveAssetTypes( assetTypes : List<AllAssetTypes>)
//
//    @Query( "delete from assetsTypes")
//    fun nukeAssetsTypes()
//
//    @Query("select * from assetsTypes")
//    fun getLocalAssetTypes() : LiveData<List<AllAssetTypes>>
//
//
//    // Batches
//    @Insert(onConflict = REPLACE)
//    fun saveBatches( batches: List<AllBatches>)
//
//    @Query("delete from batches")
//    fun nukeBatches()
//
//    @Query("select * from batches")
//    fun getLocalBatches() : LiveData<List<AllBatches>>
//
//
//    // Assets
//    @Insert(onConflict = REPLACE)
//    fun saveAssets( assets: List<AllAssets>)
//
//    @Query("delete from assets")
//    fun nukeAssets()
//
//    @Query("select * from assets")
//    fun getLoclAssets() : LiveData<List<AllAssets>>


    //user data
//    @Insert(onConflict = REPLACE)
//    fun saveUserData(user: User)
//
//    @Query("delete from user")
//    fun nukeUserData()
//
//    @Query("select * from user")
//    fun getUserData() : LiveData<User>


    //Contacts
    @Insert(onConflict = REPLACE)
    fun saveContactLocal(contact : List<Contact>)

    @Query("delete from contacts")
    fun nukeContacts()

    @Query("select * from contacts")
    fun getLocalContacts() : LiveData<List<Contact>>


    //Contact Groups
    @Insert(onConflict = REPLACE)
    fun saveContactGroupsLocal(contactGroup: List<ContactGroup>)

    @Query("delete from contactGroups")
    fun nukeContactGroups()

    @Query("select * from contactGroups")
    fun getLocalContactGroups(): LiveData<List<ContactGroup>>


    //categories
    @Insert(onConflict = REPLACE)
    fun saveCategoriesLocal(category: List<Category>)

    @Query("delete from categories")
    fun nukeCategories()

    @Query("select * from categories")
    fun getLocalCategories(): LiveData<List<Category>>


    //PRODUCTS
    @Insert(onConflict = REPLACE)
    fun saveProductsLocal(product: List<Product>)

    @Query("delete from products")
    fun nukeProducts()

    @Query("select * from products")
    fun getLocalProducts(): LiveData<List<Product>>


    //SALES
    @Insert (onConflict = REPLACE)
    fun addItem(salesLocal: SalesLocal)

    @Query("UPDATE sales SET quantity = :quantity WHERE id = :pid")
    fun updateItem(quantity: Int, pid: Int): Int

    @Query("select * from sales where id = :id")
    fun checkItemExists(id:Int): SalesLocal?

    @Query("select * from sales")
    fun getAllSales():LiveData<List<SalesLocal>>


}