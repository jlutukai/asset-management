package com.example.assetmanagement.model

data class UserData(
    val user : User,
    val contact : Contact?,
    val status_code : Int
)