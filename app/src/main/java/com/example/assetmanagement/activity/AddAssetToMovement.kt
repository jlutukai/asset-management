package com.example.assetmanagement.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.assetmanagement.R
import com.example.assetmanagement.model.AllStations
import com.example.assetmanagement.model.Assetss
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class AddAssetToMovement : AppCompatActivity() {

    private lateinit var stations : List<AllStations>
    private lateinit var assets : List<Assetss>
    private  var allstations : ArrayList<AllStations> = arrayListOf()
    private  var allassets : ArrayList<Assetss> = arrayListOf()
    private lateinit var userstation : String
    private lateinit var contactID:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_asset_to_movement)
        sortList()
    }

    private fun sortList() {
        GlobalScope.launch(Dispatchers.Main) { checklist()}
    }

    private suspend fun checklist() {
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful){
                stations = response.body()!!.stations
                assets = response.body()!!.assets

            }
        }catch (e:IOException){

        }
        try {
            val response = ApiClient.webService.getUserData(getToken())
            if (response.isSuccessful){
                if (response.body()!!.user.is_customer == 0) {
                    stations = response.body()!!.user.stations
                    val uid =response.body()!!.user.id
                    for (i in stations){
                        if (i.id == i.pivot.business_location_id && uid == i.pivot.user_id){
                            userstation =i.pivot.business_location_id.toString()
                        }else{
                            contactID = response.body()!!.contact.toString()
                        }
                        }
                    }
            }
        }catch (e:IOException){
            //error
        }
        for (i in assets){
            if (i.current_station_id.toString() == userstation){
                allassets.add(i)
            }else if (i.current_contact_id.toString() == contactID){
                allassets.add(i)
            }
        }

    }
}
