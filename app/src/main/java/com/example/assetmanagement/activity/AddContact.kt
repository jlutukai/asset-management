package com.example.assetmanagement.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.assetmanagement.R
import com.example.assetmanagement.model.ContactGroup
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.room.viewModel.GeneralViewModel
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.isNetworkConnected
import com.example.assetmanagement.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_add_contact.*
import kotlinx.android.synthetic.main.activity_add_contact.progress
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moe.feng.common.stepperview.VerticalStepperItemView
import java.io.IOException
import java.util.*

class AddContact : AppCompatActivity() {
    private val permissionRequestAccessFineLocation = 100
    private lateinit var uriContacts: Uri
    private val mSteppers = arrayOfNulls<VerticalStepperItemView>(4)
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var contactList: MutableList<ContactGroup>
    private var contactGroupNames: MutableList<String> = arrayListOf()
    private var contactTypes: MutableList<String> =
        arrayListOf("None", "Supplier", "Customer", "Both")
    private var biz: MutableList<String> = arrayListOf("None", "Company", "Individual")
    private var genders: MutableList<String> = arrayListOf("None", "Male", "Female", "Other")
    private var name: String = ""
    private var id: String = ""
    private var isIndividual = 1
    private var contactType = ""
    private var firstName = ""
    private var lastName = ""
    private var surName = ""
    private var idNumber = ""
    private var businessName = ""
    private var dateOfBirth = ""
    private var date = ""
    private var tax = ""
    private var taxNoInd = ""
    private var gender = ""
    private var companyName = ""
    private var taxNoComp = ""
    private var companyRegNo = ""
    private var dateOfReg = ""
    private var email = ""
    private var phone = ""
    private var alternate = ""
    private var landline =""
    private var lat : String =""
    private var long : String = ""
    private var physicalAddress :String? = null
    private var postal :String?= null
    private var city :String?= null
    private var country :String?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_contact)

        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getContactGroups()
        getLocationDetails()

        mSteppers[0] = findViewById(R.id.stepper_0)
        mSteppers[1] = findViewById(R.id.stepper_1)
        mSteppers[2] = findViewById(R.id.stepper_2)
        mSteppers[3] = findViewById(R.id.stepper_3)
        VerticalStepperItemView.bindSteppers(*mSteppers)

        PushDownAnim.setPushDownAnimTo(next_1_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                firstSteps()
            }
        PushDownAnim.setPushDownAnimTo(next_2_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                secondStep()
            }
        PushDownAnim.setPushDownAnimTo(back_2_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            mSteppers[1]!!.prevStep()
        }
        PushDownAnim.setPushDownAnimTo(next_3_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            thirdStep()
        }
        PushDownAnim.setPushDownAnimTo(back_3_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            mSteppers[2]!!.prevStep()
        }
        PushDownAnim.setPushDownAnimTo(back_4).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            mSteppers[3]!!.prevStep()
        }
        PushDownAnim.setPushDownAnimTo(finish_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            if (address_contact.text.toString().trim() != null){
            physicalAddress = address_contact.text.toString().trim()}
            if (postal_contact.text.toString().trim()  != null){
            postal = postal_contact.text.toString().trim()}
            if (city_contact.text.toString().trim() != null){
            city = city_contact.text.toString().trim()}
            if (country_contact.text.toString().trim() != null){
            country = country_contact.text.toString().trim()}
            lastStep()
        }

        dob.setOnClickListener {
            openCalender()
        }
        dor.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, yr, monthOfYear, dayOfMonth ->
                    val d = "$dayOfMonth-${monthOfYear.plus(1)}-$yr"
                    dor.setText(d)
                },
                year-25,
                month,
                day
            )
            dpd.show()
        }

    }
    private fun openCalender() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { view, yr, monthOfYear, dayOfMonth ->
                val d = "$dayOfMonth-${monthOfYear.plus(1)}-$yr"
                dob.setText(d)
            },
            year-25,
            month,
            day
        )
        dpd.show()
    }


    private fun firstSteps() {
        when {
            contact_groups.selectedItem == "None" -> {
                showToast("select contact group")
                return
            }
            contact_types.selectedItem == "None" -> {
                showToast("select contact type")
                return
            }
            contact_individual.selectedItem == "None" -> {
                showToast("select contact type")
                return
            }
            else -> {
                if (contact_individual.selectedItem == "Company") {
                    isIndividual = 0
                }
                if (contact_individual.selectedItem == "Individual") {
                    isIndividual = 1
                }
                for (i in contactList) {
                    if (i.name == contact_groups.selectedItem) {
                        id = i.id.toString()
                    }
                }
                contactType = contact_types.selectedItem.toString()
                if (isIndividual == 1) {
                    if (contactType == "Supplier"){
                        biiz.visibility = View.VISIBLE
                    }else{
                        biiz.visibility = View.GONE
                    }
                    individual_details.visibility = View.VISIBLE
                    company_details.visibility = View.GONE
                    mSteppers[0]!!.nextStep()
                }
                if (isIndividual == 0) {
                    biiz.visibility = View.GONE
                    company_details.visibility = View.VISIBLE
                    individual_details.visibility = View.GONE
                    mSteppers[0]!!.nextStep()
                }
            }
        }
    }

    private fun secondStep() {
        firstName = first_name.text.toString().trim()
        lastName = last_name.text.toString().trim()
        surName = surname.text.toString().trim()
        idNumber = id_no_contact.text.toString().trim()
        dateOfBirth = dob.text.toString().trim()
        taxNoInd = tax_no.text.toString().trim()
        businessName = business_name_contact.text.toString().trim()
        gender = gender_contact.selectedItem.toString()
        companyName = company_name.text.toString().trim()
        companyRegNo = company_reg_no.text.toString().trim()
        taxNoComp = tax_no_comp.text.toString().trim()
        dateOfReg = dor.text.toString().trim()

        if (isIndividual == 1) {
            when {
                firstName.isEmpty() -> {
                    first_name.error = "Required"
                    return
                }
                lastName.isEmpty() -> {
                    last_name.error = "Required"
                    return
                }
                surName.isEmpty() -> {
                    surname.error = "Required"
                    return
                }
                idNumber.isEmpty() -> {
                    id_no_contact.error = "Required"
                    return
                }
                dateOfBirth.isEmpty() -> {
                    dob.error = "Required"
                    return
                }
                gender == "None" -> {
                    showToast("Please specify gender")
                    return
                }
                else -> {
                    name = "$surName $firstName $lastName"
                    tax = taxNoInd
                    date = dateOfBirth
                    mSteppers[1]!!.nextStep()
                }
            }
        }
        if (isIndividual == 0) {
            when {
                companyName.isEmpty() -> {
                    company_name.error = "Required"
                    return
                }
                dateOfReg.isEmpty() -> {
                    dor.error = "Required"
                    return
                }
                else -> {
                    name = companyName
                    tax =taxNoComp
                    date = dateOfReg
                    mSteppers[1]!!.nextStep()
                }
            }
        }
    }

    private fun thirdStep() {
        email = email_contact.text.toString().trim()
        phone = mobile_contact.text.toString().trim()
        alternate = alternate_contact.text.toString().trim()
        landline = landline_contact.text.toString().trim()

        if (phone.isEmpty()){
            mobile_contact.error = "Required"
            return
        }else{
            mSteppers[2]!!.nextStep()
        }
    }
    private fun lastStep() {

        GlobalScope.launch(Dispatchers.Main) { postContact() }
    }

    @SuppressLint("Recycle")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            uriContacts = data!!.data!!
            contactPicked()
        }

    }

    @SuppressLint("Recycle")
    private fun contactPicked() {
        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        )
        val resolver = contentResolver
        val cursor = resolver.query(uriContacts, projection, null, null, null)
        cursor!!.moveToFirst()
        val indexNo = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
        val phoneNumber = getString(indexNo)
        val indexNa = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
        val nameCont = getString(indexNa)
        cursor.close()



    }

    private suspend fun postContact() {
        try {
            val response =
                ApiClient.webService.addContact(getToken(), name, email, phone,idNumber,physicalAddress, id, businessName,isIndividual,date,firstName,lastName,
                    surName,gender,contactType,tax,city,country,landline,alternate,postal,long,lat)
            if (response.isSuccessful) showToast("success")
            progress.visibility = View.GONE
            startActivity(Intent(this@AddContact, Contacts::class.java).putExtra("id", id))
            finish()
        } catch (e: IOException) {
            //error
        }
    }

    private fun getContactGroups() {
        contactList = arrayListOf()
        if (isNetworkConnected(this)) {
            generalViewModel.getUpdatedContactGroups()
        } else {
            showToast("from Room db")
        }
        generalViewModel.getLocalContactGroups()
            .observe(this@AddContact, Observer { contactGroups ->
                if (contactGroups.isNotEmpty()) {
                    contactList.clear()
                    progress.visibility = View.GONE
                    contactList.addAll(contactGroups)
                    contactGroupNames.clear()
                    contactGroupNames.add("None")
                    contactGroupNames.add("Customers")
                    for (i in contactList) {
                        contactGroupNames.add(i.name)
                    }
                    val adapter: ArrayAdapter<String> = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        contactGroupNames as List<String>
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    contact_groups.adapter = adapter
                } else {
                    showToast("No Contact Groups Available")
                }

            })
        val adapter1: ArrayAdapter<String> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            contactTypes as List<String>
        )
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        contact_types.adapter = adapter1
        val adapter2: ArrayAdapter<String> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            biz as List<String>
        )
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        contact_individual.adapter = adapter2
        val adapter3: ArrayAdapter<String> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            genders as List<String>
        )
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        gender_contact.adapter = adapter3
    }

    private fun getLocationDetails() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                this as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),permissionRequestAccessFineLocation)
            return
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val locationListener =  object  : LocationListener {

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {

            }

            override fun onLocationChanged(p0: Location?) {
                val latitude = p0!!.latitude
                lat = latitude.toString()
                val longitude = p0.longitude
                long = longitude.toString()
            }
        }


        locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestAccessFineLocation) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocationDetails()
                PackageManager.PERMISSION_DENIED -> showToast("enable location")
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
