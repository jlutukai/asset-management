package com.example.assetmanagement.model

data class Contacts(
    val contacts: List<Contact>
)