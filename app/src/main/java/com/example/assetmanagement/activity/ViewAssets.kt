package com.example.assetmanagement.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.google.gson.Gson
import com.example.assetmanagement.adapter.ViewAssetAdapter
import com.example.assetmanagement.model.*
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.isNetworkConnected
import com.example.assetmanagement.utils.showToast
import kotlinx.android.synthetic.main.activity_view_assets.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class ViewAssets : AppCompatActivity() {
    private val permissionRequestAccessFineLocation = 100
    private lateinit var allMovements: List<Movements>
    private lateinit var allassets : List<Assetss>
    private var cid: String? = null
    private var sid: String? = null
    private lateinit var token : String
    private lateinit var tc : String
    private var id : String = ""
    private lateinit var ts : String
    private lateinit var fs : String
    private lateinit var stations: List<AllStations>
    private lateinit var status: List<String>
    private lateinit var mstatus: List<String>
    private lateinit var currentState: String
    private lateinit var s: String
    private lateinit var movement : Movements
    private lateinit var user : User
    private  var contact : Contact? = null
    private var lat : String =""
    private var long : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_assets)

        setSupportActionBar(toolbar_va)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        id = intent.getStringExtra("id")!!
        getLocationDetails()
        showToast(id)
        token = getToken()

        rv_asset_trf.layoutManager = LinearLayoutManager(this)
        rv_asset_trf.isNestedScrollingEnabled = false

        getData(id)
    }

    private fun getLocationDetails() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                this as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),permissionRequestAccessFineLocation)
            return
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val locationListener =  object  : LocationListener {

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {

            }

            override fun onLocationChanged(p0: Location?) {
                val latitude = p0!!.latitude
                lat = latitude.toString()
                val longitude = p0.longitude
                long = longitude.toString()
            }
        }


        locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestAccessFineLocation) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocationDetails()
                PackageManager.PERMISSION_DENIED -> showToast("enable location")
            }
        }
    }

    fun refresh(id: String) {
        progress_va.visibility =View.VISIBLE
        showToast("Status Changed  Successfully")
        getData(id)
    }

    fun getData(id : String) {
        if (isNetworkConnected(this)) {
            GlobalScope.launch(Dispatchers.Main) { getAllData(id) }
        } else {
            showToast("Check Internet Connection")
        }
    }

    private suspend fun getAllData(id : String) {
        allassets = arrayListOf()
        val stids: MutableList<String> = arrayListOf()
        try{
            val response = ApiClient.webService.getUserData(getToken())
            if (response.isSuccessful){
                user = response.body()!!.user
                contact = response.body()!!.contact
            }
        }catch (e: IOException){

        }
        try {
            val response = ApiClient.webService.fetchAssetTransfers(token)
            if (response.isSuccessful) {
                allMovements = response.body()!!.movements

                status = response.body()!!.asset_movement_status
                mstatus = response.body()!!.movement_status
            }
            for (i in allMovements) {

                if (i.id.toString() == id) {
                    s = i.status.toString()
                    allassets = i.assets
                    movement = i
                }
            }

            currentState = status[s.toInt()]
        } catch (e: IOException) {

        }
        try {
            val response = ApiClient.webService.getUserData(token)
            if (response.isSuccessful) {
                if (response.body()!!.user.is_customer == 0) {
                    stations = response.body()!!.user.stations
                    val uid = response.body()!!.user.id
                    for (i in stations) {
                        if (i.id == i.pivot.business_location_id && uid == i.pivot.user_id) {
//                          stationNames.add(i.name +" "+ i.landmark )
                            sid = i.pivot.business_location_id.toString()
                        }
                    }
                } else {
                    cid = response.body()!!.contact.toString()
                }
            }
        } catch (e: IOException) {

        }
        stids.clear()
        for (i in user.stations){
            stids.add(i.id.toString())
        }
        val adapter = ViewAssetAdapter(this, allassets as ArrayList<Assetss>,status,user,movement,contact,stids)

        if (s.toInt() == 0) {

            all.visibility = View.VISIBLE
            all.text = "Dispatch All"
            all.setOnClickListener {
//                val dialog = Dialog(this)
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//                dialog.setCancelable(true)
//                dialog.setContentView(R.layout.transporters_dialog)
                val s = "1"
                for (i in allassets){
                    changeStatus(s,i.id.toString(), id)
                }

            }

        }
        if (contact != null){
            if (s.toInt() == 1 && movement.to_contact_id == contact!!.id) {
                all.visibility = View.VISIBLE
                allr.visibility = View.VISIBLE
                allr.text = "Reject All"
                all.text = "Receive all"
                allr.setOnClickListener {
                    val s = "3"
                    for (i in allassets){
                        changeStatus(s,i.id.toString(), id)
                    }
                }
                all.setOnClickListener {
                    val s = "2"
                    for (i in allassets){
                        changeStatus(s,i.id.toString(), id)
                    }
                }
            }
        }
        if (user.stations.isNotEmpty()){
            if (s.toInt() == 1 &&  stids.contains(movement.to_station_id.toString())) {
                all.visibility = View.VISIBLE
                allr.visibility = View.VISIBLE
                allr.text = "Reject All"
                all.text = "Receive all"
                allr.setOnClickListener {
                    val s = "3"
                    for (i in allassets){
                        changeStatus(s,i.id.toString(), id)
                    }
                }
                all.setOnClickListener {
                    val s = "2"
                    for (i in allassets){
                        changeStatus(s,i.id.toString(), id)
                    }
                }
            }
        }

        showToast(allassets.size.toString()+" "+allMovements.size.toString() )
        if (allassets.isEmpty()){
            showToast("No Assets Available currently...")
        }else{
            rv_asset_trf.adapter = adapter
            adapter.onItemClickRecieve = {
                changeStatu("2",it.id.toString(),id)
            }
            adapter.onItemClickReject = {
                changeStatu("3", it.id.toString(), id)
            }
        }

        progress_va.visibility = View.GONE
    }
    fun changeStatus(s: String, aid : String,id : String)  {
        GlobalScope.launch(Dispatchers.Main) {
            if(change(s, aid, id)){
                progress_va.visibility = View.VISIBLE
        getData(id)
        }else{
            showToast("failed")
        }
        }
    }

    private suspend fun change(s: String,aid : String,id : String) : Boolean {
        try {
            val response = ApiClient.webService.changeStatus(getToken(),id,aid,lat,long,s)
            if (response.isSuccessful){
                showToast("Success")
                return true
            }else{
                val gson = Gson()
                val errorResponse = gson.fromJson<Message>(response.errorBody()!!.charStream(),
                    Message::class.java)
                val msg = errorResponse.message
                showToast(msg)
                return false
            }
        }catch (e:IOException){

        }
        return false
    }
    fun changeStatu(s: String, aid : String,  id : String) {
        GlobalScope.launch(Dispatchers.Main) {
            if (change(s, aid)){
                progress_va.visibility = View.VISIBLE
//            startActivity(Intent(this, ViewAssets::class.java).putExtra("id", id))
                getData(id)
            }else{
                showToast("failed")
            }
        }
    }
    private suspend fun change(s: String,aid : String) : Boolean{
        try {
            val response = ApiClient.webService.changeStatus(getToken(),id,aid, lat, long,s)
            return if (response.isSuccessful){
                true
            }else{
                val gson = Gson()
                val errorResponse = gson.fromJson<Message>(response.errorBody()!!.charStream(),
                    Message::class.java)
                val msg = errorResponse.message
                showToast(msg)
                false
            }
        }catch (e:IOException){

        }
        return false
    }

    override fun onStart() {
        super.onStart()
        token = getToken()
    }

    override fun onResume() {
        super.onResume()
        token = getToken()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
