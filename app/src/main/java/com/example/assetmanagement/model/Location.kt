package com.example.assetmanagement.model

data class Location(
    val id : Int,
    val business_id : Int,
    val location_type_id : Int,
    val code : String,
    val name : String,
    val slug : String
)