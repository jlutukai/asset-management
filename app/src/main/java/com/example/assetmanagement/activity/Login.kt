package com.example.assetmanagement.activity

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.multidex.MultiDex
import com.example.assetmanagement.R
import com.google.gson.Gson
import com.example.assetmanagement.model.Message
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.showToast
import com.example.assetmanagement.utils.storeToken
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.*
import java.io.IOException

class Login : AppCompatActivity() {

    private lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_login)

        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.loading_dialog)

        PushDownAnim.setPushDownAnimTo(create_account).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(this, RegisterContact::class.java))
        }
        PushDownAnim.setPushDownAnimTo(login_btn).setScale(PushDownAnim.MODE_STATIC_DP,8F).setOnClickListener {
            checkPerm()
        }




    }

    private fun checkPerm() {
        askPermission(Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION){
            //all permissions already granted or just granted
            dialog.show()
            GlobalScope.launch (Dispatchers.Main){
                loginUser()
            }
        }.onDeclined { e ->
            if (e.hasDenied()) {
                //the list of denied permissions
//                e.denied.forEach {
//                }

                AlertDialog.Builder(this)
                    .setMessage("Please accept our permissions")
                    .setPositiveButton("yes") { dialog, which ->
                        e.askAgain();
                    } //ask again
                    .setNegativeButton("no") { dialog, which ->
                        dialog.dismiss();
                    }
                    .show();
            }

//            if(e.hasForeverDenied()) {
                //the list of forever denied permissions, user has check 'never ask again'
//                e.foreverDenied.forEach {
//                }
                // you need to open setting manually if you really need it
//                e.goToSettings();
//            }
        }
    }

    private suspend fun loginUser()  {
        val name = username.text.toString().trim()
        val pass = password.text.toString().trim()

       if (name.isEmpty()){
           username.error = "REQUIRED"
           dialog.dismiss()
           return
       }
       if (pass.isEmpty()){
           password.error = "REQUIRED"
           dialog.dismiss()
           return
       }

       //No empty fields
       try {
           val response = ApiClient.webService.loginUser(name,pass)
//           val response = ApiClient.webService.posts()
           if (response.code() == 200){
               dialog.dismiss()
                val token  = response.body()
               token?.let { storeToken("Bearer ${token.token}")
                   val intent = Intent(this@Login, Main2Activity::class.java)
               startActivity(intent)
               }
           }else {
               dialog.dismiss()
               val gson = Gson()
               val errorResponse = gson.fromJson<Message>(response.errorBody()!!.charStream(),
                   Message::class.java)
               val msg = errorResponse.error
               showToast(msg)
           }
       }catch (e: IOException){
           dialog.dismiss()
           showToast("Error : ${e.message}")
//           Log.d("EXCEPTION", e.message!!)
       }
    }
//


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }}
//    override fun onStart() {
//        super.onStart()
////        val token:String? = getToken()
////        token?.let {
////            if (token != null){
////                startActivity(Intent(this, CategoriesActivity::class.java))
////            }
////        }
//    }
//}
