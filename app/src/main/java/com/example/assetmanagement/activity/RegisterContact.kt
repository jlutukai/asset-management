package com.example.assetmanagement.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.assetmanagement.R
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_register_contact.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class RegisterContact : AppCompatActivity() {
    private var lat : String =""
    private var long : String = ""
    private val permissionRequestAccessFineLocation = 100
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_register_contact)
        getLocationDetails()
        PushDownAnim.setPushDownAnimTo(sign_up_btn).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            doInBg()
        }
        PushDownAnim.setPushDownAnimTo(signin_register).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(this, Login::class.java))
            finish()
        }
    }

    private fun doInBg() {
        GlobalScope.launch(Dispatchers.Main) { registerUser() }
    }

    private suspend fun registerUser() {
        val uname = username_register.text.toString().trim()
        val fname = fname_register.text.toString().trim()
        val lname = lname_register.text.toString().trim()
        val phone = phone_register.text.toString().trim()
        val pass = password_register.text.toString().trim()
        val pass2 = cpassword_register.text.toString().trim()

        if (uname.isEmpty()){
            username_register.error = "Required"
            return
        }
        if (fname.isEmpty()){
            fname_register.error = "Required"
            return
        }
        if (lname.isEmpty()){
            lname_register.error = "Required"
            return
        }
        if (phone.isEmpty()){
            phone_register.error = "Required"
            return
        }
        if (pass.isEmpty()){
            password_register.error = "Required"
            return
        }
        if (pass2.isEmpty()){
            cpassword_register.error ="Required"
            return
        }
        if (pass2 != pass){
            password_register.error = "Do not Match"
            cpassword_register.error ="Do not Match"
            return
        }

        try {
            val response = ApiClient.webService.registerNewUser(getToken(), "application/json",6,uname,fname,lname,phone,pass,long, lat)
            if (response.isSuccessful){
                showToast(response.body()!!.message)
                startActivity(Intent(this, Login::class.java))
                finish()
            }
        }catch (e:IOException){

        }
    }
    private fun getLocationDetails() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                this as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),permissionRequestAccessFineLocation)
            return
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val locationListener =  object  : LocationListener {

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {

            }

            override fun onLocationChanged(p0: Location?) {
                val latitude = p0!!.latitude
                lat = latitude.toString()
                val longitude = p0.longitude
                long = longitude.toString()
            }
        }


        locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestAccessFineLocation) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocationDetails()
                PackageManager.PERMISSION_DENIED -> showToast("enable location")
            }
        }
    }
}
