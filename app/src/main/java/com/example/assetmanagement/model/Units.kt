package com.example.assetmanagement.model

data class Units(
    val id : Int,
    val actual_name: String,
    val short_name : String
)