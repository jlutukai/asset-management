package com.example.assetmanagement.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.assetmanagement.R
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken
import com.example.assetmanagement.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_edit_contact.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class EditContact : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_contact)

        setSupportActionBar(toolbar_edit)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)

        val id = intent.getStringExtra("id")
        val name = intent.getStringExtra("name")
        val email = intent.getStringExtra("email")
        val phone = intent.getStringExtra("number")
        val biz = intent.getStringExtra("biz")

        if (!name.isNullOrEmpty()){
            name_edit.setText(name)
        }
        if (!email.isNullOrEmpty()){
            mail_edit.setText(email)
        }
        if (!phone.isNullOrEmpty()){
            phone_edit.setText(phone)
        }
        if (!biz.isNullOrEmpty()){
            business_name_edit.setText(biz)
        }
        PushDownAnim.setPushDownAnimTo(update_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            updateContact()
        }
    }

    private fun updateContact() {
        GlobalScope.launch(Dispatchers.Main) { postUpdatedContacts() }
    }

    private suspend fun postUpdatedContacts() {
        val id = intent.getStringExtra("id")
        val email : String = mail_edit.text.toString()
        val name = name_edit.text.toString()
        val phone = phone_edit.text.toString()
        val biz = business_name_edit.text.toString()
        try {
            val response = ApiClient.webService.updateContact(getToken(),id!!,name,email,phone,biz)
            if (response.isSuccessful){
                this.onBackPressed()
                showToast("Updated Successfully")
            }
        }catch (e:IOException){

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
