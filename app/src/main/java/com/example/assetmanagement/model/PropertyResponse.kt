package com.example.assetmanagement.model


data class PropertyResponse(
    val location_types : List<LocationType>,
    val locations : List<Location>,
    val property_types: List<PropertyType>,
    val properties : List<Property>
)