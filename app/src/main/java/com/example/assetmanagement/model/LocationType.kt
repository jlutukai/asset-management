package com.example.assetmanagement.model

data class LocationType(
    val id : Int,
    val business_id : Int,
    val name : String,
    val slug : String
)