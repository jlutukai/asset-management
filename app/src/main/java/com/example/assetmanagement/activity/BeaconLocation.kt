package com.example.assetmanagement.activity

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assetmanagement.R
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.assetmanagement.adapter.BeaconsAdapter
import com.example.assetmanagement.ble.BTLEDevice
import com.example.assetmanagement.ble.BroadcastReceiverBTState
import com.example.assetmanagement.ble.ScannerLocation
import com.example.assetmanagement.ble.Utils
import com.example.assetmanagement.model.Assetss
import com.example.assetmanagement.retrofit.ApiClient
import com.example.assetmanagement.utils.getToken

import com.example.assetmanagement.utils.showToast
import com.neovisionaries.bluetooth.ble.advertising.ADPayloadParser
import com.neovisionaries.bluetooth.ble.advertising.IBeacon
import kotlinx.android.synthetic.main.activity_beacon_location.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class BeaconLocation : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener {
    @kotlin.jvm.JvmField
    val REQUEST_ENABLE_BT = 1
    val BTLE_SERVICES = 2
    private lateinit var mMap: GoogleMap
    private val LOCATION_PERMISSION_REQUEST_CODE = 1
    private lateinit var lastLocation: Location
    private lateinit var mfusedLocation: FusedLocationProviderClient
    private val TAG = BeaconLocation::class.java.simpleName
    private var mBTDevicesHashMap: HashMap<String, BTLEDevice>? = null
    private var mBTDevicesArrayList: ArrayList<BTLEDevice> = arrayListOf()
    private var mBTDevices: ArrayList<BTLEDevice> = arrayListOf()
    private var assetsBT: ArrayList<BTLEDevice> = arrayListOf()
    private val macs = ArrayList<String>()
    private val proxy = ArrayList<String>()
    private var btn_Scan: Button? = null
    private var adapter: BeaconsAdapter? = null
    private var mBTStateUpdateReceiver: BroadcastReceiverBTState? = null
    private var mBTLeScanner: ScannerLocation? = null
    private lateinit var alltassets: MutableList<Assetss>
    private lateinit var assets: List<Assetss>
    private  var allassets : ArrayList<String> = arrayListOf()
    private var allassetsins: ArrayList<String> = arrayListOf()
    private val selectedMacs: ArrayList<String> = ArrayList()
    private var userstation: String = ""
    private var contactID: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beacon_location)

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Utils.toast(applicationContext, "BLE Not Supported")
        }
        mBTStateUpdateReceiver = BroadcastReceiverBTState(applicationContext)
        mBTLeScanner = ScannerLocation(this, 90000000, -15)

        mBTDevicesHashMap = HashMap()
        mBTDevicesArrayList = ArrayList()
        mBTDevices = ArrayList()
        adapter = BeaconsAdapter(this, mBTDevicesArrayList)
        rv_scanned_beacons.layoutManager = LinearLayoutManager(this)
        rv_scanned_beacons.isNestedScrollingEnabled = false
        rv_scanned_beacons.adapter = adapter
        getData()
        mfusedLocation = LocationServices.getFusedLocationProviderClient(this)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btn_Scan = findViewById(R.id.btn_scan)
        this.btn_Scan!!.setOnClickListener(this)
    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) {
            getAllData()
        }
    }

    private suspend fun getAllData() {
        val id = intent.getStringExtra("id")
        showToast(id!!)
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful)
                assets = response.body()!!.assets

            for (i in assets){
                if (i.current_station_id.toString() == id){
                    allassets.add(i.serial)
                }
            }
            mBTDevices.clear()
            for (i in mBTDevicesArrayList) {
                if (allassets.contains(i.address)) {
                    mBTDevices.add(i)
                }else if (allassets.isEmpty()){
                    showToast("No Asset Available ")
                }
            }
            showToast(mBTDevicesArrayList.size.toString()+" "+allassets.size)
            rv_scanned_beacons.adapter!!.notifyDataSetChanged()
        }catch (e:IOException){
            //error
        }
    }

    private fun startInBg() {
        if (!mBTLeScanner!!.isScanning) {
        GlobalScope.launch(Dispatchers.Main) {
            startScan()
        }}
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        // Add a marker in Sydney and move the camera
        setUpMap()
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        mfusedLocation.lastLocation.addOnSuccessListener(this) { location ->
//            mMap.isMyLocationEnabled = true
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17f))
                val markerOptions = MarkerOptions().position(currentLatLng)
                mMap.addMarker(markerOptions)
            }

        }


    }
    override fun onStart() {
        super.onStart()

        registerReceiver(
            mBTStateUpdateReceiver,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
    }

    override fun onPause() {
        super.onPause()

        //        unregisterReceiver(mBTStateUpdateReceiver);
        stopScan()
    }

     fun stopScan() {
         mBTLeScanner!!.stop()

    }
    fun startScan(){
        mBTDevicesArrayList.clear()
        mBTDevicesHashMap!!.clear()

        mBTLeScanner!!.start()

    }

    override fun onStop() {
        super.onStop()

        unregisterReceiver(mBTStateUpdateReceiver)
        stopScan()
    }

    public override fun onDestroy() {
        super.onDestroy()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                //                Utils.toast(getApplicationContext(), "Thank you for turning on Bluetooth");
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Utils.toast(applicationContext, "Please turn on Bluetooth")
            }
        } else if (requestCode == BTLE_SERVICES) {
            // Do something
        }
    }
    override fun onClick(v: View) {

        when (v.id) {

            R.id.btn_scan -> {
                Utils.toast(applicationContext, "Scan Button Pressed")

                if (!mBTLeScanner!!.isScanning) {

                    GlobalScope.launch(Dispatchers.Main) { startScan() }

                } else {
                    GlobalScope.launch(Dispatchers.Main) { stopScan() }
                }
            }
            else -> {
            }
        }

    }

    fun addDevice(device: BluetoothDevice, new_rssi: Int, scanRecord: ByteArray) {
        val structures = ADPayloadParser.getInstance().parse(scanRecord)
        for (structure in structures) {
            if (structure is IBeacon) {
                val uuid = structure.uuid
                val major = structure.major
                val minor = structure.minor
                val power = structure.power
                val address = device.address
                val dist = Utils.calculateAccuracy(power, new_rssi.toDouble())
                if (!mBTDevicesHashMap?.containsKey(address)!!) {
                    val btleDevice = BTLEDevice(device, structure, false, dist)
                    btleDevice.setRSSI(new_rssi)

                    mBTDevicesHashMap?.set(address, btleDevice)
                    mBTDevicesArrayList.add(btleDevice)
                    macs.add(address)
                    rv_scanned_beacons.adapter!!.notifyDataSetChanged()
                } else {
                    mBTDevicesHashMap!![address]!!.setRSSI(new_rssi)
                    rv_scanned_beacons.adapter!!.notifyDataSetChanged()
                }
            }
        }


        adapter!!.notifyDataSetChanged()
    }
    }


