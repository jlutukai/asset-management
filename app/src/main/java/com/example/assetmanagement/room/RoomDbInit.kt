package com.example.assetmanagement.room

import android.app.Application
import androidx.room.Room

class RoomDbInit : Application() {
    companion object {
        lateinit var database: AppDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database =
            Room.databaseBuilder(applicationContext, AppDatabase::class.java, "pos_db")
//                .allowMainThreadQueries()//to remove after testing
                .fallbackToDestructiveMigration()
                .build()
    }
}