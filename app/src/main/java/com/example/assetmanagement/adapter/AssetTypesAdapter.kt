package com.example.assetmanagement.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assetmanagement.R
import com.example.assetmanagement.activity.Batches
import com.example.assetmanagement.model.AllAssetTypes
import kotlinx.android.synthetic.main.item_contact_group.view.*

class AssetTypesAdapter (val context: Context, private val assetTypes: List<AllAssetTypes>) :
RecyclerView.Adapter<AssetTypesAdapter.AssetTypesViewModel>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetTypesViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_contact_group, parent, false)
        return AssetTypesViewModel(v)
    }

    override fun getItemCount(): Int {
       return assetTypes.size
    }

    override fun onBindViewHolder(holder: AssetTypesViewModel, position: Int) {
        val assetType = assetTypes[position]
        holder.setData(assetType, position)
    }
    inner class AssetTypesViewModel(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var current: AllAssetTypes? = null
        private var currentPos: Int = 0
        init {
            itemView.main_wrapper.setOnClickListener {
                current.let {
                    val intent = Intent(context, Batches::class.java)
                        intent.putExtra("id", current!!.id.toString() )
                    context.startActivity(intent)
                }
            }
        }
        fun setData(assetType: AllAssetTypes, position: Int) {
            assetType.let {
                itemView.cust_group.text = assetType.name
            }
            current = assetType
            currentPos = position
        }

    }

}