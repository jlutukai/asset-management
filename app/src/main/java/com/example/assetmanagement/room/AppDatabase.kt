package com.example.assetmanagement.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.assetmanagement.model.*

@Database(entities = [
    Category::class, Product::class,
    SalesLocal::class, ContactGroup::class,
    Contact::class
], version = 11)
abstract class AppDatabase : RoomDatabase() {
    abstract fun generalDao(): GeneralDao
}