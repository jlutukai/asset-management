package com.example.assetmanagement.ble;

import android.bluetooth.BluetoothDevice;

import com.neovisionaries.bluetooth.ble.advertising.IBeacon;

public class BTLEDevice {
    private BluetoothDevice device;
    private IBeacon iBeacon;
    private int rssi;
    private Boolean checked;
    private double dist;

    public BTLEDevice(BluetoothDevice device, IBeacon iBeacon, Boolean checked, Double dist){
        this.device = device;
        this.iBeacon = iBeacon;
        this.checked = checked;
        this.dist = dist;
    }
    public String getAddress(){return device.getAddress();}
    public String getTx(){return String.valueOf(iBeacon.getPower());}
    public String getUuid(){return String.valueOf(iBeacon.getUUID());}
    public void setDist(double dist){this.dist = dist; }
    public Double getDist(){return dist;}
    public void setChecked(boolean checked){this.checked =checked;}
    public Boolean getChecked(){return checked;}
    public String getMinor(){return String.valueOf(iBeacon.getMinor());}
    public void setMinor(Integer minor){
        iBeacon.setMinor(minor); }
    public String getMajor(){return String.valueOf(iBeacon.getMajor());}
    public String getName() { return device.getName();}
    public void setRSSI(int rssi){this.rssi = rssi;}
    public int getRssi() {return rssi; }
}
