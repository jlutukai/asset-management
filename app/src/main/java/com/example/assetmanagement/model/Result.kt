package com.example.assetmanagement.model

data class Result(
    val user : UserDetails,
    val contact: Contact
)