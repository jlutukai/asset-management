package com.example.assetmanagement.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contactGroups")
data class ContactGroup(
    @PrimaryKey
    val id : Int,
    val name : String,
    val updated_at : String
){constructor(): this(id = 0,name = "",updated_at = "")}