package com.example.assetmanagement.ble;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.assetmanagement.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

public class BTLEListAdapter extends ArrayAdapter<BTLEDevice> {
    private Activity activity;
    private int layoutID;
    private ArrayList<BTLEDevice> devices;
    private ArrayList<String> macs = new ArrayList<>();
    public BTLEListAdapter(Activity activity, int resource, ArrayList<BTLEDevice> objs){
        super(activity.getApplicationContext(), resource, objs);
        this.activity = activity;
        layoutID = resource;
        devices = objs;
    }
    @NotNull
    @Override
    public View getView(int pos, View convertView, @NotNull ViewGroup parent){
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(layoutID, parent, false);
        }
        BTLEDevice device = devices.get(pos);
        String name = device.getTx();
        String address = device.getAddress();
        int rssi = device.getRssi();
        String distance = String.valueOf(device.getDist());
        String d;
        if (distance.length() > 4){
            d = distance.substring(0,4);
        }else {
            d = distance;
        }
//        CheckBox ch = convertView.findViewById(R.id.select);
//        if (ch.isChecked()){
//            device.setChecked(true);
//            notifyDataSetChanged();
//        }else {
//            device.setChecked(false);
//            notifyDataSetChanged();
//        }
//        if (device.getChecked()){
//            ch.setChecked(true);
//            notifyDataSetChanged();
//        }
        TextView tv = null;

        tv = convertView.findViewById(R.id.dist);
        tv.setText(d +"m");

//        tv = convertView.findViewById(R.id.power);
//        tv.setText("Tx : "+ device.getTx()+" dBm");

//        tv = convertView.findViewById(R.id.major);
        tv.setText("Major : " + device.getMajor());

        tv = convertView.findViewById(R.id.minor);
        tv.setText("Minor : " + device.getMinor());

//        tv = convertView.findViewById(R.id.tv_name);
//        if (name.length() > 0) {
//            tv.setText("UUID : "+device.getUuid());
//        }
//        else {
//            tv.setText("No Name");
//        }

//        tv = convertView.findViewById(R.id.tv_rssi);
//        tv.setText("RSSI : " + rssi+" dBm");

        tv = convertView.findViewById(R.id.tv_macaddr);
        if (address != null && address.length() > 0) {
            tv.setText("MAC : "+device.getAddress());
        }
        else {
            tv.setText("No Address");
        }
        return convertView;
    }

    public ArrayList<String> getCheckedItems(){
        for (BTLEDevice device : devices){
            if (device.getChecked()){
                macs.add(device.getAddress());
            }
        }
        return macs;
    }
}
