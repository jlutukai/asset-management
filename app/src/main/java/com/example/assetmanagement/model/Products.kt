package com.example.assetmanagement.model

import com.example.assetmanagement.model.Product

data class Products(
    val products:List<Product>
)